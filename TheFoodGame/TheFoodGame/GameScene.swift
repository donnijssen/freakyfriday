//
//  GameScene.swift
//  TheFoodGame
//
//  Created by Don Nijssen on 17-12-16.
//  Copyright © 2016 Don Nijssen. All rights reserved.
//

import SpriteKit
import GameplayKit
import UserNotifications

class GameScene: SKScene {
    var money = 0
    var stockItems: [StockItem] = []
    var stockItemConfigurations = [String: [String: NSNumber]]()
    
    var moneyLabel = SKLabelNode(fontNamed: "TrebuchetMS-Bold")
    
    var customer:Customer?
    var timeOfLastCustomer:CFAbsoluteTime = CFAbsoluteTimeGetCurrent()
    var timeTillNextCustomer:CFTimeInterval!
    
    let hitSound = SKAction.playSoundFileNamed("hit.wav", waitForCompletion: false)
    let coinSound = SKAction.playSoundFileNamed("coin.wav", waitForCompletion: false)
    
    override func didMove(to view: SKView) {
        NotificationCenter.default.addObserver(self, selector: #selector(saveGameData), name: NSNotification.Name(rawValue: "saveGameData"), object: nil)
        
        // Draw background
        let background = SKSpriteNode(imageNamed: "bg_foodgame")
        background.position = CGPoint(x: size.width/2, y: size.height/2)
        background.zPosition = CGFloat(ZPosition.background.rawValue)
        addChild(background)
        
        // Draw HUD in top right corner that displays the amount of money the player has
        let moneyBackground = SKSpriteNode(imageNamed: "bg_money")
        moneyBackground.position = CGPoint(x: size.width - moneyBackground.size.width/2 - 10, y: size.height - moneyBackground.size.height/2 - 13)
        moneyBackground.zPosition = CGFloat(ZPosition.HUDBackground.rawValue)
        addChild(moneyBackground)
        
        moneyLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.right
        moneyLabel.verticalAlignmentMode = SKLabelVerticalAlignmentMode.top
        moneyLabel.position = CGPoint(x: size.width - 60, y: size.height - 115)
        moneyLabel.fontColor = SKColor(red: 156/255.0, green: 179/255.0, blue: 207/255.0, alpha: 1.0)
        moneyLabel.fontSize = 50
        moneyLabel.zPosition = CGFloat(ZPosition.HUDForeground.rawValue)
        addChild(moneyLabel)
        
        loadGameData()
        determineTimeTillNextCustomer()
    }
    
    // MARK: - Load and save plist file
    func saveGameData() {
        let path = documentFilePath(fileName: "gamedata.plist")
        let stockItemData = NSMutableArray()
        for stockItem : StockItem in stockItems {
            stockItemData.add(stockItem.data())
        }
        var stockItemConfigurationsObjects = [AnyObject]()
        var stockItemConfigurationsKeys = [NSCopying]()
        for (key, stockItemConfiguration) in stockItemConfigurations {
            stockItemConfigurationsKeys.append(key as NSCopying)
            stockItemConfigurationsObjects.append(stockItemConfiguration as AnyObject)
        }
        
        let stockItemConfigurationsNSDictionary = NSDictionary(objects: stockItemConfigurationsObjects, forKeys: stockItemConfigurationsKeys)
        let objects = [stockItemConfigurationsNSDictionary, money, stockItemData] as [Any]
        let keys = ["stockItemConfigurations", "money", "stockItemData"]
        let gameData = NSDictionary(objects: objects, forKeys: keys as [NSCopying])
        gameData.write(toFile: path, atomically: true)
    }
    
    func loadGameData() {
        var path = documentFilePath(fileName: "gamedata.plist")
        var gameData : NSDictionary? = NSDictionary(contentsOfFile: path)
        // Load gamedata template from mainBundle if no saveFile exists
        if gameData == nil {
            let mainBundle = Bundle.main
            path = mainBundle.path(forResource: "gamedata", ofType: "plist")!
            gameData = NSDictionary(contentsOfFile: path)
        }
        
        stockItemConfigurations = gameData!["stockItemConfigurations"] as! [String: [String: NSNumber]]
        money = gameData!["money"] as! Int
        moneyLabel.text = String(format: "%i $", money)
        let stockItemDataSet = gameData!["stockItemData"] as! [[String: AnyObject]]
        for stockItemData in stockItemDataSet {
            let itemType = stockItemData["type"] as AnyObject? as! String
            let stockItemConfiguration = stockItemConfigurations[itemType] as [String: NSNumber]!
            let stockItem  = StockItem(stockItemData: stockItemData, stockItemConfiguration: stockItemConfiguration!, gameDelegate: self)
            let relativeX = stockItemData["x"] as AnyObject? as! Float
            let relativeY = stockItemData["y"] as AnyObject? as! Float
            stockItem.position = CGPoint(x: Int(relativeX * Float(size.width)), y: Int(relativeY * Float(size.height)))
            addChild(stockItem)
            stockItems.append(stockItem)
        }
    }
    
    func documentFilePath(fileName: String) -> String {
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let fileURL = documentsURL.appendingPathComponent(fileName)
        return fileURL.path
    }
    
    override func update(_ currentTime: TimeInterval) {
        for stockItem in stockItems {
            stockItem.update()
        }
        
        // 1
        let currentTimeAbsolute = CFAbsoluteTimeGetCurrent()
        if customer == nil && currentTimeAbsolute - timeOfLastCustomer > timeTillNextCustomer {
            // 2
            var potentialWishes: [StockItem] = []
            for stockItem in stockItems {
                if stockItem.state == .selling || stockItem.state == .stocked {
                    potentialWishes.append(stockItem)
                }
            }
            // 3
            if potentialWishes.count > 0 {
                let random = arc4random_uniform(UInt32(potentialWishes.count))
                let randomStockItem = potentialWishes[Int(random)]
                customer = Customer(type: randomStockItem.type, flavor: randomStockItem.flavor)
                customer!.position = CGPoint(x: frame.size.width + customer!.calculateAccumulatedFrame().size.width / 2, y: customer! .calculateAccumulatedFrame().size.height / 2)
                // 4
                let moveLeft = SKAction.move(by: CGVector(dx: -customer!.calculateAccumulatedFrame().size.width, dy: 0), duration: 1)
                customer?.run(moveLeft)
                addChild(customer!)
            }
        }
    }
    
    func determineTimeTillNextCustomer() {
        timeTillNextCustomer = CFTimeInterval(Float((arc4random_uniform(UInt32(15)) + 15)) * TimeScale)
    }
    
    func serveCustomerWithItemOfType(type: String, flavor: String) {
        if customer?.type == type && customer?.flavor == flavor {
            let _ = updateMoney(by: 50)
            let playSound = SKAction.playSoundFileNamed("coin.wav", waitForCompletion: true)
            run(playSound)
        } else {
            let playSound = SKAction.playSoundFileNamed("hit.wav", waitForCompletion: true)
            run(playSound)
        }
        if customer != nil {
            let moveRight = SKAction.move(by: CGVector(dx: customer!.calculateAccumulatedFrame().size.width, dy: 0), duration: 1)
            customer!.run(moveRight, completion: {
                self.customer?.removeFromParent()
                self.customer = nil
            })
            timeOfLastCustomer = CFAbsoluteTimeGetCurrent()
            determineTimeTillNextCustomer()
        }
    }
}

// MARK: - GameDelegate
extension GameScene: GameDelegate {
    
    func updateMoney(by delta: Int) -> Bool {
        if money + delta < 0 {
            return false
        }
        let deltaLabel = SKLabelNode(fontNamed: "TrebuchetMS-Bold")
        if delta < 0 {
            deltaLabel.fontColor = SKColor(red: 198/255.0, green: 139/255.0, blue: 207/255.0, alpha: 1.0)
        } else {
            deltaLabel.fontColor = SKColor(red: 156/255.0, green: 179/255.0, blue: 207/255.0, alpha: 1.0)
        }
        deltaLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.right
        deltaLabel.verticalAlignmentMode = SKLabelVerticalAlignmentMode.bottom
        deltaLabel.position = moneyLabel.position
        deltaLabel.text = String(format: "%i $", delta)
        deltaLabel.fontSize = moneyLabel.fontSize
        deltaLabel.zPosition = CGFloat(ZPosition.HUDForeground.rawValue)
        addChild(deltaLabel)
        
        let moveLabelAction = SKAction.move(by: CGVector(dx: 0, dy: 20), duration: 0.5)
        let fadeLabelAction = SKAction.fadeOut(withDuration: 0.5)
        let labelAction = SKAction.group([moveLabelAction, fadeLabelAction])
        deltaLabel.run(labelAction, completion: {deltaLabel.removeFromParent()})
        
        money += delta
        moneyLabel.text = String(format: "%i $", money)
        
        return true
    }
    
}

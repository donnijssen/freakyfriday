//
//  ProgressBar.swift
//  TheFoodGame
//
//  Created by Don Nijssen on 20-12-16.
//  Copyright © 2016 Don Nijssen. All rights reserved.
//

import SpriteKit

protocol ProgressBar {
    
    var node: SKNode { get }
    func setProgress(percentage: Float)
    
}

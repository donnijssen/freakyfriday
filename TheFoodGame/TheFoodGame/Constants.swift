//
//  Constants.swift
//  TheFoodGame
//
//  Created by Don Nijssen on 20-12-16.
//  Copyright © 2016 Don Nijssen. All rights reserved.
//

enum ZPosition: Int {
    case background
    case stockItemsBackground
    case stockItemsForeground
    case HUDBackground
    case HUDForeground
}

enum State: Int{
    case empty
    case stocking
    case stocked
    case selling
}

let TimeScale: Float = 1


//
//  GameDelegate.swift
//  TheFoodGame
//
//  Created by Don Nijssen on 20-12-16.
//  Copyright © 2016 Don Nijssen. All rights reserved.
//

protocol GameDelegate {
    
    func updateMoney(by delta: Int) -> Bool
    
    func serveCustomerWithItemOfType(type: String, flavor: String)
    
}

<?php

define("DEBUG",true);

function db_connect()
{
    // Define connection as a static variable, to avoid connecting more than once
    static $connection;

    // Try and connect to the database, if a connection has not been established yet
    if (!isset($connection)) {
        /* // Load configuration as an array. Use the actual location of your configuration file
         $connection = mysqli_connect('rdbms.strato.de','U2693112','fiesta123!','DB2693112');*/

        $servername = "rdbms.strato.de";
        $dbName = "DB2693112";
        $username = "U2693112";
        $password = "fiesta123!";

        try {
            $connection = new PDO("mysql:host=$servername;dbname=$dbName", $username, $password);
            // set the PDO error mode to exception
            $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            if (DEBUG) {
                echo "Connection failed: " . $e->getMessage();
            }
        }

    }

    // If connection was not successful, handle the error
    if ($connection === false) {
        echo "CONNECTION FALSE";
        // Handle error - notify administrator, log to a file, show an error screen, etc.
        //return mysql_connect_error();
    }

    return $connection;
}

function db_query($query)
{
    // Connect to the database
    $connection = db_connect();
    // Query the database
    try {
        $result = $connection->query($query);
    } catch (PDOException $e) {
        if (DEBUG) {
            echo "Exception: " . $e;
        }
    }
    return $result;
}

function db_execute($query)
{
    $connection = db_connect();
    try {
        $connection->exec($query);
        return true;
    } catch (PDOException $e) {
        if (DEBUG) {
            echo "Exception: " . $e;
        }
        return false;
    }
}

function db_queryParams($query, $params)
{
    $connection = db_connect();
    $result = $connection->prepare($query);
    for ($i = 1; $i <= count($params); $i++) {

        echo "i: " . $i;
        $p = $params[$i - 1];
        echo "p: " . $p;
        $result->bindParam($i, $p);
    }

    var_dump($result);
    return $result->execute();
}

function db_error()
{
    echo "error";
    $connection = db_connect();
    return mysqli_error($connection);
}

?>
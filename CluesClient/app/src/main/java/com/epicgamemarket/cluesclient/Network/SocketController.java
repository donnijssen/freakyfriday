package com.epicgamemarket.cluesclient.Network;

import android.content.Intent;
import android.util.Log;

import com.epicgamemarket.cluesclient.GameData.Server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

import static com.epicgamemarket.cluesclient.GameData.Server.SERVERPORT;

/**
 * Created by dnijs on 10/10/2016.
 */

public class SocketController {
    public static void sendMsgToServer(String msg,Server s) throws IOException {
        OutputStream outputStream = s.getSocket().getOutputStream();
        PrintStream printStream = new PrintStream(outputStream);
        printStream.print(msg);
        //printStream.close();
    }
}

package com.epicgamemarket.cluesclient.Network;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.epicgamemarket.cluesclient.Controller.GameController;
import com.epicgamemarket.cluesclient.GameData.Json.CurrentPlayer;
import com.epicgamemarket.cluesclient.GameData.Json.Players;
import com.epicgamemarket.cluesclient.GameData.Player;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.util.ArrayList;


/**
 * Created by Kai on 11-10-2016.
 */

public class SController {
    Context mCon;
    Socket socket;
    private String SERVER_IP = "127.0.0.1";
    int SERVERPORT = 8000;
    private boolean connected = false;
    private static Handler reconnectHandler;

    public boolean connect(Context c,String serverIP) {
        this.mCon = c;
        this.SERVER_IP = serverIP;

        CommunicationThread commThread = null;
        try {
            commThread = new CommunicationThread(SERVER_IP, SERVERPORT);
            new Thread(commThread).start();
            connected = true;
        } catch (IOException e) {
            connected = false;
            e.printStackTrace();
        }
        return connected;
    }

    public void sendMessage(String msg) throws IOException {
        if (connected) {
            PrintWriter out = new PrintWriter(new BufferedWriter(
                    new OutputStreamWriter(socket.getOutputStream())),
                    true);
            out.println(msg);
        } else{
            Log.e("ConError","Not connected");
        }
    }

    class CommunicationThread implements Runnable {

        private Socket clientSocket;

        private BufferedReader input;

        private InetAddress serverAddr;
        private int SERVERPORT;

        public CommunicationThread(String SERVER_IP, int SERVERPORT) throws IOException {

            this.serverAddr = InetAddress.getByName(SERVER_IP);
            this.SERVERPORT = SERVERPORT;


        }

        public void run() {
            try {
                this.clientSocket = new Socket(serverAddr, SERVERPORT);
                socket = clientSocket;
                if (!clientSocket.isConnected()){
                    Log.e("BackgroundServer", "Socket is closed");
                    return;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                Log.i("BackgroundServer", "commThread started");

                PrintWriter out = new PrintWriter(new BufferedWriter(
                        new OutputStreamWriter(clientSocket.getOutputStream())),
                        true);
                out.println("connect");

                this.input = new BufferedReader(new InputStreamReader(this.clientSocket.getInputStream()));

            } catch (Exception e) { // IOException or Nullpointer because ClientSocket == null
                e.printStackTrace();
            }
            //PrintWriter out = null;
            BufferedReader in = null;
            int BUFFER_SIZE = 255;
            try {
                //out = new PrintWriter(clientSocket.getOutputStream());
                in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                int charsRead = 0;
                char[] buffer = new char[BUFFER_SIZE];
                while (true) {
                    String s = in.readLine();
                    Log.i("S from server: ", s);
                    processMessage(s);

                }
            } catch (Exception e) {
                GameController.setConnected(false);
                Intent intent = new Intent("GETDATA");
                intent.putExtra("disconnected",true);
                mCon.sendBroadcast(intent);
                e.printStackTrace();
            }
        }

        private void processMessage(String msg) {
            if (msg.equals("connected")) {
                GameController.setConnected(true);
                Intent intent = new Intent("GETDATA");
                intent.putExtra("connected",true);
                mCon.sendBroadcast(intent);
            } else if (msg.equals("started")) {
                GameController.setGameStarted(true);
            } else if (msg.equals("witch")) {
                GameController.setWitch();
            } else if (msg.equals("disconnected")) {
                GameController.setConnected(false);
            } else if (msg.equals("round_started")){
                GameController.setRoundActive(true);
                Intent intent = new Intent("GETDATA");
                intent.putExtra("roundstarted",true);
                mCon.sendBroadcast(intent);
            } else if (msg.equals("round_stopped")){
                GameController.setRoundActive(false);
                Intent intent = new Intent("GETDATA");
                intent.putExtra("roundstopped",true);
                mCon.sendBroadcast(intent);
            } else if (msg.contains("players")) {
                Gson gson = new Gson();
                Players players = gson.fromJson(msg, Players.class);
                ArrayList<Player> playerList = players.getPlayers();
                GameController.setPlayers(playerList);
                Intent intent = new Intent("GETDATA");
                intent.putExtra("players", true);
                mCon.sendBroadcast(intent);
            } else if (msg.contains("currentPlayer")) {
                Gson gson = new Gson();
                CurrentPlayer cp = gson.fromJson(msg, CurrentPlayer.class);
                Player p = cp.getCurrentPlayer();
                GameController.setCurrentPlayer(p);
                Intent intent = new Intent("GETDATA");
                intent.putExtra("currentPlayer", true);
                mCon.sendBroadcast(intent);
            } else if (msg.equals("stopped")){
                GameController.endGame();
                Intent intent = new Intent("GETDATA");
                intent.putExtra("stopped", true);
                mCon.sendBroadcast(intent);
            } else if (msg.contains("Hint")){
                Intent intent = new Intent("GETDATA");
                intent.putExtra("Hint",msg);
                mCon.sendBroadcast(intent);
            } else if (msg.equals("hb")){
                try {
                    sendMessage("hb_ok");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else if (msg.equals("hb_ok")){
                Log.i("heartbeat","Received heartbeat");
            } else if (msg.contains("Error")){
                Intent intent = new Intent("GETDATA");
                intent.putExtra("Error",msg);
                mCon.sendBroadcast(intent);
            }
        }
    }
}

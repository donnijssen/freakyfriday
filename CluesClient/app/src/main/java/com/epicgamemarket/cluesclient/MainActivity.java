package com.epicgamemarket.cluesclient;

import android.app.Activity;
import android.app.PendingIntent;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.net.DhcpInfo;
import android.net.wifi.WifiManager;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.MifareClassic;
import android.nfc.tech.Ndef;
import android.nfc.tech.NfcA;
import android.os.Handler;
import android.os.Looper;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.text.format.Formatter;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.epicgamemarket.cluesclient.Controller.GameController;
import com.epicgamemarket.cluesclient.GameData.Hint;
import com.epicgamemarket.cluesclient.NFC.NFCReader;
import com.epicgamemarket.cluesclient.Network.SController;
import com.epicgamemarket.cluesclient.Adapter.PlayerOverviewAdapter;
import com.viewpagerindicator.UnderlinePageIndicator;

import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "" ;
    public static final String MIME_TEXT_PLAIN = "text/plain";
    //controls
   // private TextView tvCharacterName;
    private TextView tv_info;
    private ImageButton leftNav;
    private ImageButton rightNav;
    //nfc reader
    private NfcAdapter nfcAdapter;
    private NFCReader nfcReader;
    //pop up scanning hint
    ProgressDialog loading;

    private int reconnectTimeout = 2*1000;

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
   // private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    private UnderlinePageIndicator titleIndicator;
    private ImageButton btn_connect;
    private SController controller;
    private boolean gameInit = false;

    private AudioManager audioManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        //set fullscreen
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
       // mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        Drawable draw = this.getResources().getDrawable(R.drawable.backgroundwitchhunt);
        mViewPager.setBackgroundResource(R.drawable.bgclient);



        //set audiomanager
        audioManager = (AudioManager)getSystemService(Context.AUDIO_SERVICE);

       // mViewPager.setAdapter(mSectionsPagerAdapter);
        tv_info = (TextView) findViewById(R.id.tv_info);
        //setup nfcadapter
        nfcAdapter = NfcAdapter.getDefaultAdapter(this);
        //check if nfc is supported
        if(nfcAdapter == null){
            //not supported
            Toast.makeText(getApplicationContext(),"NFC NOT SUPPORTED!", Toast.LENGTH_LONG);
            finish();
            return;
        }else if(!nfcAdapter.isEnabled()){
                Toast.makeText(this,
                        "NFC NOT Enabled!",
                        Toast.LENGTH_LONG).show();
                finish();
        }else {
            //start rfid
            startNFCReader(getIntent());
        }

        btn_connect = (ImageButton)findViewById(R.id.btn_connect);
        titleIndicator = (UnderlinePageIndicator) findViewById(R.id.upi);
        titleIndicator.setBackgroundResource(R.drawable.bgclient);

       // tvCharacterName = (TextView)findViewById(R.id.tvCharacterName);
        btn_connect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String address = getServerAddress().toString();

                if (!GameController.isConnected()) {
                    controller = new SController();
                    controller.connect(getApplicationContext(), address);
                    btn_connect.setImageResource(R.drawable.btndisconnecting);
                    btn_connect.setVisibility(View.VISIBLE);
                } else{
                    try {
                        controller.sendMessage("disconnect");
                        //btn_connect.setText("Verbinden");
                        btn_connect.setImageResource(R.drawable.btnconnecting);
                        btn_connect.setVisibility(View.VISIBLE);
                        mViewPager.setVisibility(View.INVISIBLE);
                        titleIndicator.setVisibility(View.GONE);
                        tv_info.setVisibility(View.VISIBLE);
                        tv_info.setTextColor(Color.RED);
                        tv_info.setText("Niet verbonden. Klik op verbinden.");
                    } catch (IOException e) {
                        e.printStackTrace();
                        btn_connect.setImageResource(R.drawable.btnconnecting);
                    }
                }
                //starting NFC TESTING SHIT
                //startNFCReader();
            }
        });
        registerReceiver(mMessageReceiver, new IntentFilter("GETDATA"));

        leftNav = (ImageButton)findViewById(R.id.left_nav);
        rightNav = (ImageButton)findViewById(R.id.right_nav);
        // Images left navigation
        leftNav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int tab = mViewPager.getCurrentItem();
                if (tab > 0) {
                    tab--;
                    mViewPager.setCurrentItem(tab);
                } else if (tab == 0) {
                    mViewPager.setCurrentItem(tab);
                }
            }
        });

        // Images right navigatin
        rightNav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int tab = mViewPager.getCurrentItem();
                tab++;
                mViewPager.setCurrentItem(tab);
            }
        });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mMessageReceiver);
        if (controller!=null && GameController.isConnected()){
            try {
                controller.sendMessage("disconnect");
             } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //getServerAddress
    private String getServerAddress(){
        //wifi
        final WifiManager wifiManager = (WifiManager) super.getSystemService(WIFI_SERVICE);
        final DhcpInfo dhcp = wifiManager.getDhcpInfo();
        final String address = Formatter.formatIpAddress((dhcp.gateway)).toString();
        return address;

    }

    @Override
    protected void onResume() {
        super.onResume();
        setupForegroundDispatch(this, nfcAdapter);
    }

    @Override
    protected void onPause() {
        stopForegroundDispatch(this, nfcAdapter);
        super.onPause();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        startNFCReader(intent);
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {

        @Override

        public void onReceive(Context context, Intent intent) {

//            String str = intent.getStringExtra("DATA");
            boolean started = intent.getBooleanExtra("connected", false);// = intent.getIntExtra("DATA", 0);
            if (started) {
                // mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(), GameController.getPlayers(),GameController.getCurrentPlayer());
                tv_info.setText("Verbonden. Begin het spel door op de server op de \"Start game\" te klikken.");
                tv_info.setTextColor(Color.parseColor("#FFA500"));
                //btn_connect.setText("Verbinding verbreken");
                btn_connect.setImageResource(R.drawable.btndisconnecting);
                btn_connect.setVisibility(View.VISIBLE);

            } else if (intent.getBooleanExtra("currentPlayer", false)) {
                if (GameController.getPlayers() != null && !GameController.getPlayers().isEmpty()) {
                    //if (!gameInit) {
                        beginGame();
                   // }
                }

            } else if (intent.getBooleanExtra("players", false)) {
                if (GameController.getCurrentPlayer() != null) {
                    //if (!gameInit) {
                        beginGame();
                    //}
                }
            }else if (intent.getBooleanExtra("roundstopped",false)){
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        MainActivity.this);
                alertDialogBuilder
                        .setMessage("Ronde is gestopt!!!")
                        .setCancelable(false)
                        .setPositiveButton("Ok",new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.dismiss();
                            }
                        }).create().show();
                audioManager.setRingerMode(AudioManager.RINGER_MODE_VIBRATE);
                Vibrator vi = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                // Vibrate for 500 milliseconds
                vi.vibrate(500);
            }
            else if (intent.getBooleanExtra("roundstarted",false)){
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                            MainActivity.this);
                    alertDialogBuilder
                            .setMessage("Ronde is gestart!!!")
                            .setCancelable(false)
                            .setPositiveButton("Ok",new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,int id) {
                                    dialog.dismiss();
                                }
                            }).create().show();

            } else if (intent.getBooleanExtra("stopped",false)){
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        MainActivity.this);
                alertDialogBuilder
                        .setMessage("Game is gestopt!")
                        .setCancelable(false)
                        .setPositiveButton("Ok",new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.dismiss();
                            }
                        }).create().show();
                //vibs end game
                audioManager.setRingerMode(AudioManager.RINGER_MODE_VIBRATE);
                Vibrator vi = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                // Vibrate for 500 milliseconds
                vi.vibrate(500);

                gameInit = false;
                mViewPager.setVisibility(View.INVISIBLE);
                mViewPager.setAdapter(null);
                titleIndicator.setVisibility(View.GONE);
                tv_info.setText("Verbonden. Begin het spel door op de server op de \"Start game\" te klikken.");
                tv_info.setTextColor(Color.parseColor("#FFA500"));
                tv_info.setVisibility(View.VISIBLE);
                btn_connect.setVisibility(View.VISIBLE);
                btn_connect.setBackgroundResource(R.drawable.btndisconnecting);
                leftNav.setVisibility(View.INVISIBLE);
                rightNav.setVisibility(View.INVISIBLE);

            } else if (intent.getStringExtra("Hint") != null && !intent.getStringExtra("Hint").isEmpty()){
                loading.dismiss();
                String[] hints = intent.getStringExtra("Hint").split("/");
                if (hints.length>=4) {
                    String hint = hints[1];
                    final String rfid = hints[2];
                    boolean enabled = hints[3].equals("enabled");
                    Hint h = new Hint(hint, rfid, enabled);
                    showHint(h);
                }
            } else if (intent.getBooleanExtra("disconnected",false)){
                //btn_connect.setText("Verbinden");
                mViewPager.setVisibility(View.INVISIBLE);
                titleIndicator.setVisibility(View.GONE);
                tv_info.setVisibility(View.VISIBLE);
                tv_info.setTextColor(Color.RED);
                tv_info.setText("Niet verbonden. Klik op verbinden.");
                btn_connect.setVisibility(View.VISIBLE);
                btn_connect.setImageResource(R.drawable.btnconnecting);
                leftNav.setVisibility(View.INVISIBLE);
                rightNav.setVisibility(View.INVISIBLE);
                tryReconnect();
            } else if (intent.getStringExtra("Error") != null &&!intent.getStringExtra("Error").isEmpty()){
                String error = intent.getStringExtra("Error").split("/")[1];
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        MainActivity.this);
                alertDialogBuilder
                        .setMessage(error)
                        .setCancelable(false)
                        .setPositiveButton("Sluiten",new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.dismiss();
                            }
                        }).create().show();
            }
        }

    };

    private void showHint(final Hint h){

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                MainActivity.this);
        alertDialogBuilder
                .setCancelable(false);
        if (GameController.isWitch()&&h.isEnabled()){
            alertDialogBuilder.setMessage("Hint: "+h.getHint()+"\nWil je dit punt uitschakelen?");
            alertDialogBuilder.setPositiveButton("Ja", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    try {
                        controller.sendMessage("disable/"+h.getRfid());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }).setNegativeButton("Nee",new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog,int id) {
                    dialog.dismiss();
                }
            });
        } else {
            alertDialogBuilder.setMessage(h.getHint()).setNegativeButton("Sluiten", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();
                }
            });
        }
        alertDialogBuilder.create().show();
    }


    private void beginGame(){

        PlayerOverviewAdapter pova = new PlayerOverviewAdapter(getApplicationContext(),GameController.getPlayers(),GameController.getCurrentPlayer());

        mViewPager.setAdapter(pova);
        titleIndicator.setViewPager(mViewPager);
        titleIndicator.setFades(false);
        pova.notifyDataSetChanged();
        if (!gameInit) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                    MainActivity.this);
            alertDialogBuilder
                    .setCancelable(false)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                        }
                    });
            if (GameController.isWitch()) {
                alertDialogBuilder.setMessage("Game is gestart.\nJij bent de heks!");
            } else {
                alertDialogBuilder.setMessage("Game is gestart.\nJij bent niet de heks!");
            }
            alertDialogBuilder.create().show();
        }
        gameInit = true;
        mViewPager.setEnabled(true);
        mViewPager.setVisibility(View.VISIBLE);
        titleIndicator.setVisibility(View.VISIBLE);
        tv_info.setVisibility(View.GONE);
        btn_connect.setVisibility(View.GONE);
        leftNav.setVisibility(View.VISIBLE);
        rightNav.setVisibility(View.VISIBLE);
        mViewPager.setCurrentItem(GameController.getPlayers().indexOf(GameController.getCurrentPlayer()));
    }

    public void sendTAG(String tag )
    {
        System.out.println("GEVONDEN" + tag);
        //Toast.makeText(getApplicationContext(),"GEVONDEN :" + tag,Toast.LENGTH_LONG).show();
    }

    private void startNFCReader(Intent intent)
    {

        loading = ProgressDialog.show(MainActivity.this, "Zoeken naar de hint...",null,true,true);

        //start scan nfc
        String action = intent.getAction();
        if (NfcAdapter.ACTION_NDEF_DISCOVERED.equals(action)) {

            String type = intent.getType();
            if (MIME_TEXT_PLAIN.equals(type)) {
                //nfc chip uitlezen
                byte[] tagId = intent.getByteArrayExtra(NfcAdapter.EXTRA_ID);
                String tagIdString = ByteArrayToHexString(tagId);
                showTagID(tagIdString);
                getHint(tagIdString);


                /*
                Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
                nfcReader = new NFCReader();
                nfcReader.setMainActivity(this);
                nfcReader.execute(tag);*/

            } else {
                Log.d(TAG, "Wrong mime type: " + type);
            }
        } else if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(action)) {
            //nfc chip uitlezen
            byte[] tagId = intent.getByteArrayExtra(NfcAdapter.EXTRA_ID);
            String tagIdString = ByteArrayToHexString(tagId);
            showTagID(tagIdString);
            //get hint
            getHint(tagIdString);


            /*
            // In case we would still use the Tech Discovered Intent
            Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
            String[] techList = tag.getTechList();
            String searchedTech = NfcA.class.getName();

            for (String tech : techList) {
                if (searchedTech.equals(tech)) {
                    nfcReader = new NFCReader();
                    nfcReader.setMainActivity(this);
                    nfcReader.execute(tag);
                    break;
                }
            }*/
        }else{
            loading.dismiss();
        }

    }


    public static void setupForegroundDispatch(final Activity activity, NfcAdapter adapter) {
        final Intent intent = new Intent(activity.getApplicationContext(), activity.getClass());
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);

        final PendingIntent pendingIntent = PendingIntent.getActivity(activity.getApplicationContext(), 0, intent, 0);

        IntentFilter[] filters = new IntentFilter[1];
        String[][] techList = new String[][]{};

        // Notice that this is the same filter as in our manifest.
        filters[0] = new IntentFilter();
        filters[0].addAction(NfcAdapter.ACTION_NDEF_DISCOVERED);
        filters[0].addCategory(Intent.CATEGORY_DEFAULT);
        try {
            filters[0].addDataType(MIME_TEXT_PLAIN);
        } catch (IntentFilter.MalformedMimeTypeException e) {
            throw new RuntimeException("Check your mime type.");
        }

        adapter.enableForegroundDispatch(activity, pendingIntent, filters, techList);
    }

    public static void stopForegroundDispatch(final Activity activity, NfcAdapter adapter) {
        adapter.disableForegroundDispatch(activity);
    }

    private String ByteArrayToHexString(byte [] inarray)
    {
        int i, j, in;
        String [] hex = {"0","1","2","3","4","5","6","7","8","9","A","B","C","D","E","F"};
        String out= "";

        for(j = 0 ; j < inarray.length ; ++j)
        {
            in = (int) inarray[j] & 0xff;
            i = (in >> 4) & 0x0f;
            out += hex[i];
            i = in & 0x0f;
            out += hex[i];
        }
        return out;
    }

    private void showTagID(String tagId){
        //Toast.makeText(getApplicationContext(),"Gevonden tagID" + tagId, Toast.LENGTH_LONG).show();
        Log.i("TAG","TAGID: "+tagId);
    }

    private void getHint(String tagID){
        try {
            controller.sendMessage("hint/" + tagID);
        }catch(IOException ioe){
        }
    }

    private void tryReconnect() {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(new Runnable() {

            @Override
            public void run() {
                String address = getServerAddress().toString();

                if (!GameController.isConnected()) {
                    controller = new SController();
                    controller.connect(getApplicationContext(), address);
                    tryReconnect();
                }
            }

        },  reconnectTimeout);

    }
}

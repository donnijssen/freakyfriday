package com.epicgamemarket.cluesclient.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.epicgamemarket.cluesclient.GameData.Player;
import com.epicgamemarket.cluesclient.R;

import java.util.ArrayList;

/**
 * Created by Kai on 11-10-2016.
 */

public class PlayerOverviewAdapter extends PagerAdapter {

    Drawable dwTop;
    Drawable dwPants;
    Drawable dwHat;
    Drawable dwShoes;
    ViewPager vp;
    private Context mContext;
    private ArrayList<Player> players;
    private Player currentPlayer;
    private int currentPlayerIndex = 0;

    public PlayerOverviewAdapter(Context c, ArrayList<Player> pl, Player cp) {
        this.mContext = c;
        this.players = pl;
        this.currentPlayer = cp;
        this.vp = vp;
        dwTop = ContextCompat.getDrawable(mContext, R.drawable.shirt);
        dwPants = ContextCompat.getDrawable(mContext, R.drawable.pants);
        dwHat = ContextCompat.getDrawable(mContext, R.drawable.hat);
        dwShoes = ContextCompat.getDrawable(mContext, R.drawable.shoes);
    }

    @Override
    public int getCount() {
        return players.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View playerLayout = inflater.inflate(R.layout.player_overview, container, false);

        Player p = players.get(position);

        ImageView iv_hat = (ImageView) playerLayout.findViewById(R.id.ivHat);
        ImageView iv_shirt = (ImageView) playerLayout.findViewById(R.id.ivTop);
        ImageView iv_pants = (ImageView) playerLayout.findViewById(R.id.ivBottom);
        ImageView iv_shoes = (ImageView) playerLayout.findViewById(R.id.ivShoe);
        TextView tv_name = (TextView) playerLayout.findViewById(R.id.tvCharacterName);


        Drawable dwTop2 = dwTop.getConstantState().newDrawable();
        Drawable dwPants2 = dwPants.getConstantState().newDrawable();
        Drawable dwHat2 = dwHat.getConstantState().newDrawable();
        Drawable dwShoes2 = dwShoes.getConstantState().newDrawable();
        dwTop2.mutate();
        dwPants2.mutate();
        dwHat2.mutate();
        dwShoes2.mutate();
        dwTop2.setColorFilter(p.getShirtAColor(), PorterDuff.Mode.SRC_ATOP);
        dwPants2.setColorFilter(p.getPantsAColor(), PorterDuff.Mode.SRC_ATOP);
        dwHat2.setColorFilter(p.getHatAColor(), PorterDuff.Mode.SRC_ATOP);
        dwShoes2.setColorFilter(p.getShoesAColor(), PorterDuff.Mode.SRC_ATOP);

        iv_shirt.setImageDrawable(dwTop2);
        iv_pants.setImageDrawable(dwPants2);
        iv_hat.setImageDrawable(dwHat2);
        iv_shoes.setImageDrawable(dwShoes2);

        String name = p.getName();
        if (p.getName().equals(currentPlayer.getName())){
            name += " - YOU";
            currentPlayerIndex = position;
        }

        tv_name.setText(name);
        container.addView(playerLayout);
        return playerLayout;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((View) object);
    }

    public int getCurrentPlayerIndex() {
        return currentPlayerIndex;
    }
}

package com.epicgamemarket.cluesclient.GameData.Json;


import com.epicgamemarket.cluesclient.GameData.Player;

/**
 * Created by Kai on 11-10-2016.
 */

public class CurrentPlayer {

    private Player currentPlayer;

    public CurrentPlayer(Player p){
        this.currentPlayer = p;
    }

    public Player getCurrentPlayer(){
        return currentPlayer;
    }
}

package fhict.bluetooth2;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.ServerSocket;
import java.net.Socket;

import fhict.bluetooth2.controllers.GameController;

/**
 * Created by Kai on 4-10-2016.
 */
//https://examples.javacodegeeks.com/android/core/socket-core/android-socket-example/
public class BackgroundServer extends IntentService {

    public static final int SERVERPORT = 8000;
    Thread serverThread = null;
    private ServerSocket serverSocket;
    private GameController gameController;

    public BackgroundServer() {
        super("BackgroundServer");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.i("BgServer", "onHandleIntent");
        this.serverThread = new Thread(new BackgroundServer.ServerThread());
        this.serverThread.start();
        Serializable object = intent.getSerializableExtra("controller");
        if (object instanceof GameController){
            gameController = (GameController)object;
        }else{
            //TODO error.
        }
    }

    class ServerThread implements Runnable {

        public void run() {
            Socket socket = null;
            try {
                serverSocket = new ServerSocket(SERVERPORT);
                Log.i("ServerInfo", "Started on ip: " + serverSocket.getInetAddress());
            } catch (IOException e) {
                e.printStackTrace();
            }
            while (!Thread.currentThread().isInterrupted()) {

                try {
                    socket = serverSocket.accept();
                    Log.i("BackgroundServer", "serverThread started");
                    BackgroundServer.CommunicationThread commThread = new BackgroundServer.CommunicationThread(socket);
                    new Thread(commThread).start();

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    class CommunicationThread implements Runnable {

        private Socket clientSocket;

        private BufferedReader input;

        public CommunicationThread(Socket clientSocket) {

            this.clientSocket = clientSocket;

            try {
                Log.i("BackgroundServer", "commThread started");
                this.input = new BufferedReader(new InputStreamReader(this.clientSocket.getInputStream()));

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public void run() {

            while (!Thread.currentThread().isInterrupted()) {

                try {
                    String read = input.readLine();
                    if (read != null && !read.isEmpty()) {
                        Log.i("BgServer", "Msg received: " + read);

                        processMessage(read, clientSocket);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        private void processMessage(String msg, Socket socket) {
            Log.i("msg_received", "Msg received: " + msg);
            if (msg.equals("connect")) {
                if (gameController.getPlayerController().addPlayer(socket)) {
                    Intent intent = new Intent("GETDATA");
                    intent.putExtra("DATA", gameController.getPlayerController().getNoOfPlayers());
                    sendBroadcast(intent);
                }
            } else if (msg.contains("hint")) {
                String[] ids = msg.split("/");
                String rfidId = ids[1];
                if (gameController.getLocationController().giveHint(socket,rfidId)) {
                    Intent intent = new Intent("GETDATA");
                    intent.putExtra("hintCollect", true);
                    sendBroadcast(intent);
                }
                //processInput(playerId, rfidId);
            } else if (msg.contains("disable")) {
                String[] ids = msg.split("/");
                String rfidID = ids[1];
                if (gameController.getLocationController().disableLocation(rfidID, socket)) {
                    Intent intent = new Intent("GETDATA");
                    intent.putExtra("locInfo", gameController.getLocationController().getLocationInfo());
                    sendBroadcast(intent);
                }
            } else if (msg.equals("disconnect")) {
                if (gameController.getPlayerController().removePlayer(socket)) {
                    Intent intent = new Intent("GETDATA");
                    intent.putExtra("DATA", gameController.getPlayerController().getNoOfPlayers());
                    sendBroadcast(intent);
                }
            } else if (msg.equals("hb_ok")) {
                //TODO heartbeat
                //AllControllerOld.heartbeatReceived(getApplicationContext(), socket);
            } else if (msg.equals("hb")) {
                //AllControllerOld.respondHeartbeat(socket);
            }

        }

    }
}
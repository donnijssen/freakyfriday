package fhict.bluetooth2.models;

/**
 * Created by Kai on 6-10-2016.
 */

public class Hint {
    private ClothingType clothing;
    private ClothingColor clothColor;
    private String hint;

    public Hint(ClothingType ht, ClothingColor c,String h){
        this.clothing  =ht;
        this.clothColor = c;
        this.hint = h;
    }

    public ClothingType getClothing() {
        return clothing;
    }

    public ClothingColor getClothColor() {
        return clothColor;
    }

    public String getHint() {
        return hint;
    }
}

package fhict.bluetooth2.models;

import android.graphics.Color;

import java.net.Socket;

/**
 * Created by Kai on 6-10-2016.
 */

public class Player {
    private String name;
    private ClothingColor hatColor;
    private ClothingColor shirtColor;
    private ClothingColor pantsColor;
    private ClothingColor shoesColor;

    private transient String ip;
    private transient long heartbeat;
    private transient Socket socket;

    public Player(String n, ClothingColor hat, ClothingColor shirt,ClothingColor pants, ClothingColor shoes,Socket s){
        this.name = n;
        this.hatColor = hat;
        this.shirtColor = shirt;
        this.pantsColor = pants;
        this.shoesColor = shoes;

        this.setSocket(s);
    }

    public String getName() {
        return name;
    }

    public ClothingColor getHatColor() {
        return hatColor;
    }

    public int getHatAColor(){
        if (hatColor == ClothingColor.Blue){
            return Color.BLUE;
        } else if (hatColor == ClothingColor.Green){
            return Color.GREEN;
        } else if (hatColor == ClothingColor.Purple){
            return Color.parseColor("#551A8B");
        } else if (hatColor == ClothingColor.Red){
            return Color.RED;
        } else if (hatColor == ClothingColor.Yellow){
            return Color.YELLOW;
        }
        return Color.WHITE;
    }

    public ClothingColor getShirtColor() {
        return shirtColor;
    }

    public int getShirtAColor(){
        if (shirtColor == ClothingColor.Blue){
            return Color.BLUE;
        } else if (shirtColor == ClothingColor.Green){
            return Color.GREEN;
        } else if (shirtColor == ClothingColor.Purple){
            return Color.parseColor("#551A8B");
        } else if (shirtColor == ClothingColor.Red){
            return Color.RED;
        } else if (shirtColor == ClothingColor.Yellow){
            return Color.YELLOW;
        }
        return Color.WHITE;
    }

    public ClothingColor getPantsColor() {
        return pantsColor;
    }

    public int getPantsAColor(){
        if (pantsColor == ClothingColor.Blue){
            return Color.BLUE;
        } else if (pantsColor == ClothingColor.Green){
            return Color.GREEN;
        } else if (pantsColor == ClothingColor.Purple){
            return Color.parseColor("#551A8B");
        } else if (pantsColor == ClothingColor.Red){
            return Color.RED;
        } else if (pantsColor == ClothingColor.Yellow){
            return Color.YELLOW;
        }
        return Color.WHITE;
    }

    public ClothingColor getShoesColor() {
        return shoesColor;
    }

    public int getShoesAColor(){
        if (shoesColor == ClothingColor.Blue){
            return Color.BLUE;
        } else if (shoesColor == ClothingColor.Green){
            return Color.GREEN;
        } else if (shoesColor == ClothingColor.Purple){
            return Color.parseColor("#551A8B");
        } else if (shoesColor == ClothingColor.Red){
            return Color.RED;
        } else if (shoesColor == ClothingColor.Yellow){
            return Color.YELLOW;
        }
        return Color.WHITE;
    }

    public Socket getSocket() {
        return socket;
    }

    public void setSocket(Socket socket) {
        this.socket = socket;
    }

    public String getIp() {
        return ip;
    }

    public long getHeartbeat() {
        return heartbeat;
    }

    public String getIpNoPort(){
        return ip.split(":")[0];
    }
    //

}

package fhict.bluetooth2.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import fhict.bluetooth2.adapters.PlayerRecViewAdapter;
import fhict.bluetooth2.controllers.GameController;
import fhict.bluetooth2.MainTabActivity;
import fhict.bluetooth2.R;

public class PlayersFragment extends Fragment {
    RecyclerView rv;
    TextView tv_error;
    private GameController gameController;

    public PlayersFragment() {
        // Required empty public constructor
    }

    public static PlayersFragment newInstance() {
        PlayersFragment fragment = new PlayersFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void setMenuVisibility(final boolean visible) {
        super.setMenuVisibility(visible);
        if (visible) {
            updatePlayersList();

        }
    }

    private void updatePlayersList() {
        gameController = ((MainTabActivity)getActivity()).getGameController();
        Log.i("PF","update players list");
        if (gameController.getPlayerController().getPlayers() != null && !gameController.getPlayerController().getPlayers().isEmpty()) {
            PlayerRecViewAdapter prva = new PlayerRecViewAdapter(getContext(), gameController.getPlayerController().getPlayers());
            rv.setAdapter(prva);
            rv.setLayoutManager(new LinearLayoutManager(getContext()));
            rv.setVisibility(View.VISIBLE);
            tv_error.setVisibility(View.GONE);
        } else {
            tv_error.setVisibility(View.VISIBLE);
            rv.setVisibility(View.GONE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_players, container, false);
        tv_error = (TextView) v.findViewById(R.id.tv_errorNoPlayers);
        rv = (RecyclerView) v.findViewById(R.id.rv_players);
        updatePlayersList();
        return v;
    }
}

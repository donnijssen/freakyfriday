package fhict.bluetooth2.fragments;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import fhict.bluetooth2.BackgroundServer;
import fhict.bluetooth2.controllers.GameController;
import fhict.bluetooth2.MainTabActivity;
import fhict.bluetooth2.R;

public class ControlsFragment extends Fragment {

    Button startServer;
    Button startGame;
    Button endGame;
    TextView tv_players;
    TextView tv_server;
    TextView tv_game;
    TextView tv_round;
    Button startRound;
    EditText et_roundSecs;
    GameController gameController;

    int secsElapsedInRound = 0;

    boolean serverStarted = false;

    public ControlsFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

        getContext().registerReceiver(mMessageReceiver, new IntentFilter("GETDATA"));
        // Required empty public constructor
        gameController = ((MainTabActivity)getActivity()).getGameController();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_controls, container, false);

        startServer = (Button) v.findViewById(R.id.btn_startServer);
        startGame = (Button) v.findViewById(R.id.btn_startGame);
        endGame = (Button) v.findViewById(R.id.btn_endGame);
        tv_players = (TextView) v.findViewById(R.id.tv_players);
        tv_server = (TextView) v.findViewById(R.id.tv_server);
        tv_game = (TextView) v.findViewById(R.id.tv_game);
        startRound = (Button) v.findViewById(R.id.btn_startRound);
        et_roundSecs = (EditText) v.findViewById(R.id.et_roundSecs);
        tv_round = (TextView) v.findViewById(R.id.tv_round);

        startServer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent server = new Intent(getContext(), BackgroundServer.class);

                getContext().startService(server);
                serverStarted = true;
                tv_server.setText("Server is actief");
                tv_server.setTextColor(Color.GREEN);
                startGame.setEnabled(true);
                startServer.setEnabled(false);

                heartbeat();
            }
        });

        startGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (serverStarted) {
                    if (gameController.startGame()) {
                        tv_game.setText("Game is gestart");
                        tv_game.setTextColor(Color.GREEN);
                        endGame.setEnabled(true);
                        startGame.setEnabled(false);
                        startRound.setEnabled(true);
                        startRound.setText("Start ronde "+(gameController.getCurrentRound()+1));
                        et_roundSecs.setEnabled(true);
                    } else {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                               getContext());
                        alertDialogBuilder
                                .setCancelable(false)
                                .setPositiveButton("Ok",new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,int id) {
                                        dialog.dismiss();
                                    }
                                });
                        if (gameController.getPlayerController().getNoOfPlayers()< gameController.getMinNoPlayers()){
                            alertDialogBuilder.setMessage("Game is niet gestart.\nEr zijn niet genoeg spelers, het minimum aantal is: "+ gameController.getMinNoPlayers());
                        } else{
                            alertDialogBuilder.setMessage("Game is niet gestart.");
                        }
                        alertDialogBuilder.create().show();
                    }
                } else{
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                            getContext());
                    alertDialogBuilder
                            .setMessage("Game is niet gestart, server is niet actief!")
                            .setCancelable(false)
                            .setPositiveButton("Ok",new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,int id) {
                                    dialog.dismiss();
                                }
                            }).create().show();

                }
            }
        });
        startRound.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(gameController.startRound(Integer.valueOf(et_roundSecs.getText().toString()))){

                    startRound.setEnabled(false);
                    et_roundSecs.setEnabled(false);
                    int remaining = gameController.getCurrentRoundTime()-secsElapsedInRound;
                    tv_round.setText("Ronde "+ gameController.getCurrentRound()+" bezig. Tijd resterend: "+remaining+" seconden");
                    tv_round.setTextColor(Color.GREEN);
                    updateRoundTime();

                    Intent intent = new Intent("GETDATA");
                    intent.putExtra("locInfo", gameController.getLocationController().getLocationInfo());
                    getActivity().sendBroadcast(intent);

                }
            }
        });

        endGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (gameController.isGameStarted()) {
                    gameController.stopGame();
                    tv_game.setText("Game is niet gestart");
                    tv_game.setTextColor(Color.WHITE);
                    startGame.setEnabled(true);
                    endGame.setEnabled(false);
                }
            }
        });

        tv_players.setText("Geen spelers verbonden.");


        return v;
    }

    private void heartbeat(){
        /*Handler handler = new Handler();
        handler.postDelayed(new Runnable() {

            @Override
            public void run() {
                try {
                    AllControllerOld.sendHeartbeat();
                    AllControllerOld.checkHeartbeat(getContext());
                    heartbeat();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }, AllControllerOld.getHeartTimeout());*/
        //TODO heartbeat
    }

    private void updateRoundTime(){
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {

            @Override
            public void run() {
                secsElapsedInRound++;
                int remaining = gameController.getCurrentRoundTime()-secsElapsedInRound;
                tv_round.setText("Ronde "+ gameController.getCurrentRound()+" bezig. Tijd resterend: "+remaining+" seconden");
                if (remaining <= 5) {
                    tv_round.setTextColor(Color.RED);
                } else if (remaining<=10){
                    tv_round.setTextColor(Color.parseColor("#FFA500"));
                } else {
                    tv_round.setTextColor(Color.GREEN);
                }
                if (!gameController.isRoundActive()||!gameController.isGameStarted()){
                    tv_round.setText("Ronde niet actief");
                    tv_round.setTextColor(Color.WHITE);
                    secsElapsedInRound = 0;
                    if (gameController.isGameStarted()) {
                        startRound.setEnabled(true);
                        startRound.setText("Start ronde "+(gameController.getCurrentRound()+1));
                        et_roundSecs.setEnabled(true);
                    }
                } else{
                    updateRoundTime();
                }
            }

        }, 1000);
    }



    @Override
    public void onDestroy() {
        super.onDestroy();
        getContext().unregisterReceiver(mMessageReceiver);
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {

        @Override

        public void onReceive(Context context, Intent intent) {

//            String str = intent.getStringExtra("DATA");
            int nop = intent.getIntExtra("DATA", 0);
            if (nop <= 0) {
                tv_players.setText("Geen spelers verbonden");
                tv_players.setTextColor(Color.WHITE);
            } else if (nop >= gameController.getMinNoPlayers()) {
                tv_players.setText("Aantal spelers: " + nop);
                tv_players.setTextColor(Color.GREEN);
            } else {
                tv_players.setText("Aantal spelers: " + nop);
                tv_players.setTextColor(Color.WHITE);
            }
        }

    };
}

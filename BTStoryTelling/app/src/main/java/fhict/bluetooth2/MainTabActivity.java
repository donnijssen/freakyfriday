package fhict.bluetooth2;

import android.app.ActionBar;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.design.widget.TabLayout;

import java.util.ArrayList;

import fhict.bluetooth2.controllers.GameController;
import fhict.bluetooth2.fragments.ControlsFragment;
import fhict.bluetooth2.fragments.PlayersFragment;
import fhict.bluetooth2.fragments.StatsFragment;

public class MainTabActivity extends AppCompatActivity {

    ViewPager viewPager;
    TabLayout tabLayout;
    ActionBar actionBar;
    private GameController gameController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_tab);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        tabLayout = (TabLayout) findViewById(R.id.sliding_tabs);
        viewPager.setOffscreenPageLimit(5);
        actionBar = getActionBar();

        gameController = new GameController();
        fhict.bluetooth2.adapters.FragmentPagerAdapter fpa = new fhict.bluetooth2.adapters.FragmentPagerAdapter(getSupportFragmentManager(),getApplicationContext(),getFragments());
        viewPager.setAdapter(fpa);
        tabLayout.setupWithViewPager(viewPager);

        // Iterate over all tabs and set the custom view
        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            TabLayout.Tab tab = tabLayout.getTabAt(i);
            tab.setCustomView(fpa.getTabView(i));
        }

    }

    public GameController getGameController(){
        return gameController;
    }

    private ArrayList<Fragment> getFragments(){
        ArrayList<Fragment> fragments = new ArrayList<>();
        fragments.add(new ControlsFragment());
        fragments.add(new StatsFragment());
        fragments.add(new PlayersFragment());
        return fragments;
    }
}

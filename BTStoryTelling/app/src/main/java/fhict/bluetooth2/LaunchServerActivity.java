package fhict.bluetooth2;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import fhict.bluetooth2.controllers.GameController;

public class LaunchServerActivity extends Activity {

    Button startServer;
    Button startGame;
    Button endGame;
    TextView tv_players;
    TextView tv_server;
    TextView tv_game;
    TextView tv_round;
    TextView tv_locations;
    TextView tv_hints;
    Button startRound;
    EditText et_roundSecs;
    Button btn_witch;

    int secsElapsedInRound = 0;
    int hintsCollected = 0;

    boolean serverStarted = false;

    GameController gameController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launch_server);

        startServer = (Button) findViewById(R.id.btn_startServer);
        startGame = (Button) findViewById(R.id.btn_startGame);
        endGame = (Button) findViewById(R.id.btn_endGame);
        tv_players = (TextView) findViewById(R.id.tv_players);
        tv_server = (TextView) findViewById(R.id.tv_server);
        tv_game = (TextView) findViewById(R.id.tv_game);
        startRound = (Button) findViewById(R.id.btn_startRound);
        et_roundSecs = (EditText) findViewById(R.id.et_roundSecs);
        tv_round = (TextView) findViewById(R.id.tv_round);
        tv_locations = (TextView) findViewById(R.id.tv_locInfo);
        tv_hints = (TextView) findViewById(R.id.tv_hintInfo);
        btn_witch = (Button) findViewById(R.id.btn_witch);

        startServer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gameController = new GameController();
                Intent server = new Intent(getApplicationContext(), BackgroundServer.class);
                server.putExtra("controller",gameController);
                getApplicationContext().startService(server);
                serverStarted = true;
                tv_server.setText("Server is actief");
                tv_server.setTextColor(Color.GREEN);
                startGame.setEnabled(true);
                startServer.setEnabled(false);
            }
        });

        startGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (serverStarted) {
                    if (gameController.startGame()) {
                        tv_game.setText("Game is gestart");
                        tv_game.setTextColor(Color.GREEN);
                        endGame.setEnabled(true);
                        startGame.setEnabled(false);
                        startRound.setEnabled(true);
                        startRound.setText("Start ronde "+(gameController.getCurrentRound()+1));
                        et_roundSecs.setEnabled(true);
                    } else {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                                LaunchServerActivity.this);
                        alertDialogBuilder
                                .setCancelable(false)
                                .setPositiveButton("Ok",new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,int id) {
                                        dialog.dismiss();
                                    }
                                });
                        if (gameController.getPlayerController().getNoOfPlayers()< gameController.getMinNoPlayers()){
                            alertDialogBuilder.setMessage("Game is niet gestart.\nEr zijn niet genoeg spelers, het minimum aantal is: "+ gameController.getMinNoPlayers());
                        } else{
                            alertDialogBuilder.setMessage("Game is niet gestart.");
                        }
                        alertDialogBuilder.create().show();
                    }
                } else{
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                            LaunchServerActivity.this);
                    alertDialogBuilder
                            .setMessage("Game is niet gestart, server is niet actief!")
                            .setCancelable(false)
                            .setPositiveButton("Ok",new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,int id) {
                                    dialog.dismiss();
                                }
                            }).create().show();

                }
            }
        });
        startRound.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(gameController.startRound(Integer.valueOf(et_roundSecs.getText().toString()))){

                    startRound.setEnabled(false);
                    et_roundSecs.setEnabled(false);
                    int remaining = gameController.getCurrentRoundTime()-secsElapsedInRound;
                    tv_round.setText("Ronde "+ gameController.getCurrentRound()+" bezig. Tijd resterend: "+remaining+" seconden");
                    tv_round.setTextColor(Color.GREEN);
                    updateRoundTime();
                    tv_locations.setText(gameController.getLocationController().getLocationInfo());
                }
            }
        });

        endGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (gameController.isGameStarted()) {
                    gameController.stopGame();
                    tv_game.setText("Game is niet gestart");
                    tv_game.setTextColor(Color.WHITE);
                    startGame.setEnabled(true);
                    endGame.setEnabled(false);
                }
            }
        });

        tv_players.setText("Geen spelers verbonden.");

        btn_witch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        LaunchServerActivity.this);
                alertDialogBuilder
                        .setCancelable(false)
                        .setPositiveButton("Ok",new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.dismiss();
                            }
                        });
                        if (gameController.isGameStarted()&& gameController.getPlayerController().getWitch()!=null){
                            alertDialogBuilder.setMessage("Heks: "+ gameController.getPlayerController().getWitch().getName());
                        } else{
                            alertDialogBuilder.setMessage("Heks is niet bekend");
                        }
                        alertDialogBuilder.create().show();
            }
        });

        /*btn_resetConnection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int nop = GameController.checkConnections();
                tv_players.setText("Aantal spelers: "+nop);
            }
        });*/

        registerReceiver(mMessageReceiver, new IntentFilter("GETDATA"));
    }

    private void updateRoundTime(){
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {

            @Override
            public void run() {
                secsElapsedInRound++;
                int remaining = gameController.getCurrentRoundTime()-secsElapsedInRound;
                tv_round.setText("Ronde "+ gameController.getCurrentRound()+" bezig. Tijd resterend: "+remaining+" seconden");
                if (remaining <= 5) {
                    tv_round.setTextColor(Color.RED);
                } else if (remaining<=10){
                    tv_round.setTextColor(Color.parseColor("#FFA500"));
                } else {
                    tv_round.setTextColor(Color.GREEN);
                }
                if (!gameController.isRoundActive()||!gameController.isGameStarted()){
                    tv_round.setText("Ronde niet actief");
                    tv_round.setTextColor(Color.WHITE);
                    secsElapsedInRound = 0;
                    if (gameController.isGameStarted()) {
                        startRound.setEnabled(true);
                        startRound.setText("Start ronde "+(gameController.getCurrentRound()+1));
                        et_roundSecs.setEnabled(true);
                    }
                } else{
                    updateRoundTime();
                }
            }

        }, 1000);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mMessageReceiver);
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {

        @Override

        public void onReceive(Context context, Intent intent) {

//            String str = intent.getStringExtra("DATA");
            int nop = intent.getIntExtra("DATA", 0);
            if (nop <= 0) {
                tv_players.setText("Geen spelers verbonden");
                tv_players.setTextColor(Color.WHITE);
            } else if (nop >= gameController.getMinNoPlayers()) {
                tv_players.setText("Aantal spelers: " + nop);
                tv_players.setTextColor(Color.GREEN);
            } else {
                tv_players.setText("Aantal spelers: " + nop);
                tv_players.setTextColor(Color.WHITE);
            }
            boolean hintCollected = intent.getBooleanExtra("hintCollect",false);
            if (hintCollected){
                hintsCollected++;
                tv_hints.setText(hintsCollected+" hint(s) gevonden. Resterende hints: "+ gameController.getLocationController().getNoOfHints());
            }

            String locationDisabled = intent.getStringExtra("locDisabled");
            if (locationDisabled!=null && !locationDisabled.isEmpty()) {
                tv_locations.setText(locationDisabled);
            }
        }

    };
}

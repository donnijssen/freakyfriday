package fhict.bluetooth2.controllers;

import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import fhict.bluetooth2.models.ClothingColor;
import fhict.bluetooth2.models.ClothingType;
import fhict.bluetooth2.models.Hint;
import fhict.bluetooth2.models.HintLoc;
import fhict.bluetooth2.models.Player;

/**
 * Created by Kai on 10-11-2016.
 */

public class LocationController {
    GameController gameController;
    private HashMap<String,HintLoc> locations;
    private ArrayList<HintLoc> roundDisabledLocations;
    private ArrayList<Hint> hints;
    private boolean witchDisabledLoc;
    private int noOfCorrectHintsFound = 0;

    public LocationController(GameController gc){
        this.gameController = gc;
    }

    public boolean giveHint(Socket s, String rfid) {
        HintLoc loc = locations.get(rfid);
        if (loc != null
                ) {
            String hint = "Hint/ " + getHint(rfid) + "/" + rfid + "/" + (loc.isEnabled() ? "enabled" : "disabled");
            if (loc.isEnabled()) {
                loc.cooldown();
            }
            try {
                SocketController.sendMsgToPlayer(hint, gameController.getPlayerController().getPlayerFromSocket(s));
                if (!hint.equals(HintLoc.getDisabledHintText())) {
                    return true;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            String hint = "Hint/ " + "Locatie niet gevonden" + "/" + rfid + "/" + "disabled";
            try {
                SocketController.sendMsgToPlayer(hint, gameController.getPlayerController().getPlayerFromSocket(s));
                if (!hint.equals(HintLoc.getDisabledHintText())) {
                    return true;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    private String getHint(String rfid){
        if (gameController.isGameStarted() && gameController.isRoundActive()) {
            HintLoc hl = getLocations().get(rfid);
            if (hl != null) {
                if (hl.isEnabled() && !roundDisabledLocations.contains(hl)) {
                    ArrayList<Hint> correctHints = new ArrayList<>();
                    for (Hint h : hints) {
                        if (h.getClothing() == hl.getType()) {
                            correctHints.add(h);
                        }
                    }
                    Random r = new Random();
                    if (correctHints.size() > 0) {
                        int randomInt = r.nextInt(correctHints.size());
                        Hint h = correctHints.get(randomInt);
                        hints.remove(h);
                        noOfCorrectHintsFound++;
                        return h.getHint();
                    } else {
                        Hint h = new Hint(null, null, HintLoc.getDisabledHintText());
                        return h.getHint();
                    }
                } else {
                    return HintLoc.getDisabledHintText();
                }
            }
            return "Locatie niet gevonden";
        } else if (!gameController.isGameStarted()) {
            return "Game is niet gestart";
        } else if (!gameController.isRoundActive()) {
            return "Ronde is niet actief";
        } else {
            return "Probleem tijdens het ophalen van de hint";
        }
    }

    public boolean disableLocation(String rfid,Socket s){
        Player p = gameController.getPlayerController().getPlayerFromSocket(s);
        if (p.equals(gameController.getPlayerController().getWitch()) && !witchDisabledLoc) {
            HintLoc hl = getLocations().get(rfid);
            hl.setDisabled();
            witchDisabledLoc = true;
            try {
                SocketController.sendMsgToPlayer(hl.getName() + " disabled", p);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return true;
        } else if (!p.equals(gameController.getPlayerController().getWitch())) {
            try {
                SocketController.sendMsgToPlayer("Error/Je bent niet de heks", p);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return false;
        } else if (witchDisabledLoc) {
            try {
                SocketController.sendMsgToPlayer("Error/Al een punt uitgeschakeld deze ronde", p);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return false;
        }
        return false;
    }

    public HintLoc getRandomLoctoDisable(){
        Random r = new Random();
        int index = r.nextInt(getLocations().size());
        HintLoc hl = getLocations().get(index);
        if (roundDisabledLocations.contains(hl)) {
            return getRandomLoctoDisable();
        } else {
            return hl;
        }
    }

    public void disableLocationsForRound(int roundNo){
        if (roundNo < getLocations().size()) {
            roundDisabledLocations.clear();
            for (int i = 0; i < roundNo; i++) {
                roundDisabledLocations.add(getRandomLoctoDisable());
            }
        }
        witchDisabledLoc = false;
    }

    public void filterHintsForWitch(Player w){
        ArrayList<Hint> hintsToRemove = new ArrayList<>();
        for (Hint h : hints) {
            if (h.getClothing() == ClothingType.Hat && h.getClothColor() == w.getHatColor()) {
                hintsToRemove.add(h);
            } else if (h.getClothing() == ClothingType.Shirt && h.getClothColor() == w.getShirtColor()) {
                hintsToRemove.add(h);
            } else if (h.getClothing() == ClothingType.Pants && h.getClothColor() == w.getPantsColor()) {
                hintsToRemove.add(h);
            } else if (h.getClothing() == ClothingType.Shoes && h.getClothColor() == w.getShoesColor()) {
                hintsToRemove.add(h);
            }
        }
        hints.removeAll(hintsToRemove);
    }

    public HashMap<String, HintLoc> getLocations() {
        return locations;
    }

    public void fillWithData(){
        fillHintLocations();
        fillHints();
    }

    private void fillHintLocations() {
        locations.clear();
        HintLoc hl1 = new HintLoc("04916072952B80", "Hint1", ClothingType.Hat); //Ryan
        HintLoc hl2 = new HintLoc("043356AAAE2D80", "Hint2", ClothingType.Pants); // Maikel K
        HintLoc hl3 = new HintLoc("044A04AAAE2D80", "Hint3", ClothingType.Shirt); // Kai
        HintLoc hl4 = new HintLoc("FD3CBA21", "Hint4", ClothingType.Shoes); // Maikel V
        locations.put(hl1.getRfid(), hl1);
        locations.put(hl2.getRfid(), hl2);
        locations.put(hl3.getRfid(), hl3);
        locations.put(hl4.getRfid(), hl4);
    }

    private void fillHints() {
        hints = new ArrayList<>();
        hints.clear();
        Hint hHa1 = new Hint(ClothingType.Hat, ClothingColor.Purple, "Heeft geen paarse pet");
        Hint hHa2 = new Hint(ClothingType.Hat, ClothingColor.Green, "Heeft geen groene pet");
        Hint hHa3 = new Hint(ClothingType.Hat, ClothingColor.Blue, "Heeft geen blauwe pet");
        Hint hHa4 = new Hint(ClothingType.Hat, ClothingColor.Red, "Heeft geen rode pet");
        Hint hHa5 = new Hint(ClothingType.Hat, ClothingColor.Yellow, "Heeft geen gele pet");

        Hint hSh1 = new Hint(ClothingType.Shirt, ClothingColor.Purple, "Heeft geen paars shirt");
        Hint hSh2 = new Hint(ClothingType.Shirt, ClothingColor.Green, "Heeft geen groen shirt");
        Hint hSh3 = new Hint(ClothingType.Shirt, ClothingColor.Blue, "Heeft geen blauw shirt");
        Hint hSh4 = new Hint(ClothingType.Shirt, ClothingColor.Red, "Heeft geen rood shirt");
        Hint hSh5 = new Hint(ClothingType.Shirt, ClothingColor.Yellow, "Heeft geen geel shirt");

        Hint hPa1 = new Hint(ClothingType.Pants, ClothingColor.Purple, "Heeft geen paarse broek");
        Hint hPa2 = new Hint(ClothingType.Pants, ClothingColor.Green, "Heeft geen groene broek");
        Hint hPa3 = new Hint(ClothingType.Pants, ClothingColor.Blue, "Heeft geen blauwe broek");
        Hint hPa4 = new Hint(ClothingType.Pants, ClothingColor.Red, "Heeft geen rode broek");
        Hint hPa5 = new Hint(ClothingType.Pants, ClothingColor.Yellow, "Heeft geen gele broek");

        Hint hSc1 = new Hint(ClothingType.Shoes, ClothingColor.Purple, "Heeft geen paarse schoenen");
        Hint hSc2 = new Hint(ClothingType.Shoes, ClothingColor.Green, "Heeft geen groene schoenen");
        Hint hSc3 = new Hint(ClothingType.Shoes, ClothingColor.Blue, "Heeft geen blauwe schoenen");
        Hint hSc4 = new Hint(ClothingType.Shoes, ClothingColor.Red, "Heeft geen rode schoenen");
        Hint hSc5 = new Hint(ClothingType.Shoes, ClothingColor.Yellow, "Heeft geen gele schoenen");

        hints.add(hHa1);
        hints.add(hHa2);
        hints.add(hHa3);
        hints.add(hHa4);
        hints.add(hHa5);

        hints.add(hSh1);
        hints.add(hSh2);
        hints.add(hSh3);
        hints.add(hSh4);
        hints.add(hSh5);

        hints.add(hPa1);
        hints.add(hPa2);
        hints.add(hPa3);
        hints.add(hPa4);
        hints.add(hPa5);

        hints.add(hSc1);
        hints.add(hSc2);
        hints.add(hSc3);
        hints.add(hSc4);
        hints.add(hSc5);
    }

    public String getLocationInfo() {
        int witchDisabled = 0;
        for (Map.Entry<String, HintLoc> entry : locations.entrySet()) {
            if (!entry.getValue().isEnabled()) {
                witchDisabled++;
            }
        }
        int totalDisabled = witchDisabled + roundDisabledLocations.size();
        return totalDisabled + " locatie(s) uitgeschakeld, " + witchDisabled + " door heks";
    }

    public int getNoOfCorrectHintsFound(){
        return noOfCorrectHintsFound;
    }

    public int getNoOfHints(){
        return hints.size();
    }
}

package fhict.bluetooth2.controllers;

import android.os.Handler;

import com.google.gson.Gson;

import java.io.IOException;
import java.io.Serializable;

import fhict.bluetooth2.models.Json.CurrentPlayer;
import fhict.bluetooth2.models.Json.Players;
import fhict.bluetooth2.models.Player;

/**
 * Created by Kai on 10-11-2016.
 */

public class GameController implements Serializable {
    private LocationController locationController = new LocationController(this);
    private PlayerController playerController = new PlayerController(this);
    private boolean gameStarted = false;
    private boolean roundActive = false;
    private int currentRoundTime = 0;
    private int currentRound = 0;
    private Handler roundHandler;
    private int minNoPlayers = 2;

    public boolean startGame(){
        if (getPlayerController().getPlayers().isEmpty() || getPlayerController().getPlayers().size() >= minNoPlayers){
            return false;
        }

        fillGameData();

        currentRound = 0;
       getPlayerController().setPlayerConfigs();

        try {
            Gson gson = new Gson();
            Players pl = new Players(getPlayerController().getPlayers());
            String playersGson = gson.toJson(pl);
            SocketController.sendMsgToAllPlayers("started", getPlayerController().getPlayers());
            SocketController.sendMsgToAllPlayers(playersGson, getPlayerController().getPlayers());
            for (Player p: getPlayerController().getPlayers()){
                CurrentPlayer cp = new CurrentPlayer(p);
                String player = gson.toJson(cp);
                SocketController.sendMsgToPlayer(player,p);
            }
            SocketController.sendMsgToPlayer("witch", getPlayerController().getWitch());
            gameStarted = true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return isGameStarted();
    }

    public boolean startRound(int time){
        if (isGameStarted()) {
            roundActive = true;
            currentRoundTime = time;
            getLocationController().disableLocationsForRound(getCurrentRound());
            currentRound = getCurrentRound() + 1;
            try {
                SocketController.sendMsgToAllPlayers("round_started", getPlayerController().getPlayers());

            } catch (IOException e) {
                e.printStackTrace();
            }
        }


        roundHandler = new Handler();
        roundHandler.postDelayed(new Runnable() {

            @Override
            public void run() {
                roundActive = false;
                currentRoundTime = -1;
                try {
                    SocketController.sendMsgToAllPlayers("round_stopped", getPlayerController().getPlayers());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }, time * 1000);

        return isRoundActive();
    }

    public boolean stopGame(){
        gameStarted = false;
        roundActive = false;
        if (roundHandler != null) {
            roundHandler.removeCallbacksAndMessages(null);
        }
        try {
            SocketController.sendMsgToAllPlayers("stopped", getPlayerController().getPlayers());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return true;
    }

    private void fillGameData(){
        getLocationController().fillWithData();
        getPlayerController().fillWithData();
    }

    public boolean isGameStarted() {
        return gameStarted;
    }

    public LocationController getLocationController() {
        return locationController;
    }

    public PlayerController getPlayerController() {
        return playerController;
    }

    public boolean isRoundActive() {
        return roundActive;
    }

    public int getCurrentRound() {
        return currentRound;
    }

    public int getMinNoPlayers() {
        return minNoPlayers;
    }

    public int getCurrentRoundTime() {
        return currentRoundTime;
    }
}

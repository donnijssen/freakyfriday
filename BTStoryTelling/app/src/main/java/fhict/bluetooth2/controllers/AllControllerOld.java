package fhict.bluetooth2.controllers;

import android.content.Context;
import android.os.Handler;
import android.util.Log;

import com.google.gson.Gson;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

import fhict.bluetooth2.models.ClothingColor;
import fhict.bluetooth2.models.Hint;
import fhict.bluetooth2.models.HintLoc;
import fhict.bluetooth2.models.ClothingType;
import fhict.bluetooth2.models.Json.CurrentPlayer;
import fhict.bluetooth2.models.Json.Players;
import fhict.bluetooth2.models.Player;

/**
 * Created by Kai on 6-10-2016.
 */

public class AllControllerOld {

    private static ConcurrentHashMap<String, Player> connectedIps = new ConcurrentHashMap<>();
    private static HashMap<String, HintLoc> locations = new HashMap<>();
    private static ConcurrentHashMap<String,Long> ipHeartbeat = new ConcurrentHashMap<>();
    private static ArrayList<Hint> hints = new ArrayList<>();
    private static ArrayList<String> names = new ArrayList<>();
    private static ArrayList<String> usedNames = new ArrayList<>();
    private static ArrayList<HintLoc> roundDisabledLocations = new ArrayList<>();

    private static ArrayList<Player> players = new ArrayList<>();
    private static ArrayList<HintLoc> locs = new ArrayList<>();
    private static Player witch;

    private static boolean gameStarted = false;
    private static int minNoPlayers = 2;
    private static boolean roundActive = false;
    private static int currentRoundTime = -1;
    private static int currentRound = 0;
    private static boolean witchDisabledLoc = false;

    private static long heartTimeout = 5*1000;

    private static int noOfCorrectHintsFound = 0;
    private static Handler roundHandler;


    public AllControllerOld() {

    }

    public static boolean startGame() {
        if (connectedIps.isEmpty()) {
            return false;
        }
        if (getPlayers().isEmpty() || getPlayers().size() < minNoPlayers) {
            return false;
        }

        fillGameData();

        currentRound = 0;

        ArrayList<Player> newPlayers = new ArrayList<>();
        usedNames.clear();
        connectedIps.clear();
        for (Player p : getPlayers()) {
            Player p2 = getRandomPlayer(p.getSocket());
            newPlayers.add(p2); // get New random players.
            connectedIps.put(p2.getSocket().getRemoteSocketAddress().toString(), p2);
        }

        players = newPlayers;

        if (witch == null || !gameStarted) {
            witch = setWitch();
        }

        try {
            Gson gson = new Gson();
            Players pl = new Players(getPlayers());
            String playersGson = gson.toJson(pl);
            SocketController.sendMsgToAllPlayers("started", getPlayers());
            SocketController.sendMsgToAllPlayers(playersGson, getPlayers());
            for (Map.Entry<String, Player> entry : connectedIps.entrySet()) {
                CurrentPlayer cp = new CurrentPlayer(entry.getValue());
                String player = gson.toJson(cp);
                SocketController.sendMsgToPlayer(player, entry.getValue());
            }
            SocketController.sendMsgToPlayer("witch", witch);
            gameStarted = true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return gameStarted;
    }

    public static int getNoOfHints() {
        return hints.size();
    }

    public static String getLocationInfo() {
        int witchDisabled = 0;
        for (Map.Entry<String, HintLoc> entry : locations.entrySet()) {
            if (!entry.getValue().isEnabled()) {
                witchDisabled++;
            }
        }
        int totalDisabled = witchDisabled + roundDisabledLocations.size();
        return totalDisabled + " locatie(s) uitgeschakeld, " + witchDisabled + " door heks";
    }

    public static boolean startRound(int time) {
        if (gameStarted) {
            roundActive = true;
            currentRoundTime = time;
            if (currentRound < locations.size()) {
                roundDisabledLocations.clear();
                for (int i = 0; i < currentRound; i++) {
                    roundDisabledLocations.add(getRandomLocationToDisable());
                }
            }
            currentRound++;
            witchDisabledLoc = false;
            try {
                SocketController.sendMsgToAllPlayers("round_started", getPlayers());

            } catch (IOException e) {
                e.printStackTrace();
            }
        }


        roundHandler = new Handler();
        roundHandler.postDelayed(new Runnable() {

            @Override
            public void run() {
                roundActive = false;
                currentRoundTime = -1;
                try {
                    SocketController.sendMsgToAllPlayers("round_stopped", getPlayers());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }, time * 1000);

        return isRoundActive();
    }

    private static HintLoc getRandomLocationToDisable() {
        Random r = new Random();
        int index = r.nextInt(locations.size());
        HintLoc hl = locs.get(index);
        if (roundDisabledLocations.contains(hl)) {
            return getRandomLocationToDisable();
        } else {
            return hl;
        }
    }

    public static boolean endGame() {
        gameStarted = false;
        roundActive = false;
        if (roundHandler != null) {
            roundHandler.removeCallbacksAndMessages(null);
        }
        try {
            SocketController.sendMsgToAllPlayers("stopped", getPlayers());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return true;
    }

    public static int getNoOfPlayers() {
        return getPlayers().size();
    }

    public static boolean addPlayer(String ip, Socket socket) {
        String ipNoPort = ip.split(":")[0];
        String ipToRemove = null;
        for (Map.Entry<String, Player> entry : connectedIps.entrySet()) {
            if (entry.getKey().split(":")[0].equals(ipNoPort)) {
                ipToRemove = entry.getKey();
            }
        }
        if (ipToRemove != null && !ipToRemove.isEmpty()) {
            players.remove(connectedIps.get(ipToRemove));
            return reconnectPlayer(ip,ipToRemove,socket);
        }
        if (gameStarted) {
            return false;
        }
        Player p = getRandomPlayer(socket);
        connectedIps.put(ip, p);
        players.add(p);
        try {
            SocketController.sendMsgToPlayer("connected", p);
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    private static boolean reconnectPlayer(String newIp, String oldIp,Socket s) {
        Player p = connectedIps.get(oldIp);
        if (p == null || s == null){
            return false;
        }
        p.setSocket(s);
        connectedIps.remove(oldIp);
        connectedIps.put(newIp, p);
        players.add(p);
        if (gameStarted) {
            Gson gson = new Gson();
            Players pl = new Players(new ArrayList<>(getPlayers()));
            String playersGson = gson.toJson(pl);
            try {
                SocketController.sendMsgToPlayer("connected", p);
                SocketController.sendMsgToPlayer(playersGson, p);
                CurrentPlayer cp = new CurrentPlayer(p);
                String player = gson.toJson(cp);
                SocketController.sendMsgToPlayer(player, p);
                if (p.equals(witch)){
                    SocketController.sendMsgToPlayer("witch", p);
                }
                return true;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    public static boolean removePlayer(String ip) {
        if (gameStarted) {
            return false;
        }
        Player p = connectedIps.remove(ip);
        getPlayers().remove(p);
        try {
            SocketController.sendMsgToPlayer("disconnected", p);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return true;
    }

    private static Player getRandomPlayer(Socket socket) {
        ClothingColor hat = getRandomColor();
        ClothingColor shirt = getRandomColor();
        ClothingColor pants = getRandomColor();
        ClothingColor shoes = getRandomColor();
        return new Player(getRandomName(), hat, shirt, pants, shoes, socket);
    }

    private static ClothingColor getRandomColor() {
        Random r = new Random();
        int clothingColorInt = r.nextInt(ClothingColor.values().length);
        ClothingColor cc = ClothingColor.values()[clothingColorInt];
        return cc;
    }

    private static String getRandomName() {
        Random r = new Random();
        int nameInt = r.nextInt(names.size());
        String name = names.get(nameInt);
        if (!usedNames.contains(name)) {
            usedNames.add(name);
            return name;
        } else {
            return getRandomName();
        }
    }

    private static String getHint(String rfid) {
        if (gameStarted && isRoundActive()) {
            HintLoc hl = locations.get(rfid);
            if (hl != null) {
                if (hl.isEnabled() && !roundDisabledLocations.contains(hl)) {
                    return getHint(hl.getType()).getHint();
                } else {
                    return HintLoc.getDisabledHintText();
                }
            }
            return "Locatie niet gevonden";
        } else if (!gameStarted) {
            return "Game is niet gestart";
        } else if (!isRoundActive()) {
            return "Ronde is niet actief";
        } else {
            return "Probleem tijdens het ophalen van de hint";
        }
    }

    public static Boolean isGameStarted() {
        return gameStarted;
    }

    private static Player setWitch() {
        Random r = new Random();
        int witchInt = r.nextInt(getPlayers().size());
        Player w = getPlayers().get(witchInt);
        filterHintsForWitch(w);
        return w;
    }

    public static Player getWitch() {
        return witch;
    }

    public static void fillGameData() {
        fillHintLocations();
        fillHints();
        fillNames();
    }

    private static void fillHintLocations() {
        locations.clear();
        locs.clear();
        HintLoc hl1 = new HintLoc("04916072952B80", "Hint1", ClothingType.Hat); //Ryan
        HintLoc hl2 = new HintLoc("043356AAAE2D80", "Hint2", ClothingType.Pants); // Maikel K
        HintLoc hl3 = new HintLoc("044A04AAAE2D80", "Hint3", ClothingType.Shirt); // Kai
        HintLoc hl4 = new HintLoc("FD3CBA21", "Hint4", ClothingType.Shoes); // Maikel V
        locations.put(hl1.getRfid(), hl1);
        locations.put(hl2.getRfid(), hl2);
        locations.put(hl3.getRfid(), hl3);
        locations.put(hl4.getRfid(), hl4);
        locs.add(hl1);
        locs.add(hl2);
        locs.add(hl3);
        locs.add(hl4);
    }

    private static void fillHints() {
        hints.clear();
        Hint hHa1 = new Hint(ClothingType.Hat, ClothingColor.Purple, "Heeft geen paarse pet");
        Hint hHa2 = new Hint(ClothingType.Hat, ClothingColor.Green, "Heeft geen groene pet");
        Hint hHa3 = new Hint(ClothingType.Hat, ClothingColor.Blue, "Heeft geen blauwe pet");
        Hint hHa4 = new Hint(ClothingType.Hat, ClothingColor.Red, "Heeft geen rode pet");
        Hint hHa5 = new Hint(ClothingType.Hat, ClothingColor.Yellow, "Heeft geen gele pet");

        Hint hSh1 = new Hint(ClothingType.Shirt, ClothingColor.Purple, "Heeft geen paars shirt");
        Hint hSh2 = new Hint(ClothingType.Shirt, ClothingColor.Green, "Heeft geen groen shirt");
        Hint hSh3 = new Hint(ClothingType.Shirt, ClothingColor.Blue, "Heeft geen blauw shirt");
        Hint hSh4 = new Hint(ClothingType.Shirt, ClothingColor.Red, "Heeft geen rood shirt");
        Hint hSh5 = new Hint(ClothingType.Shirt, ClothingColor.Yellow, "Heeft geen geel shirt");

        Hint hPa1 = new Hint(ClothingType.Pants, ClothingColor.Purple, "Heeft geen paarse broek");
        Hint hPa2 = new Hint(ClothingType.Pants, ClothingColor.Green, "Heeft geen groene broek");
        Hint hPa3 = new Hint(ClothingType.Pants, ClothingColor.Blue, "Heeft geen blauwe broek");
        Hint hPa4 = new Hint(ClothingType.Pants, ClothingColor.Red, "Heeft geen rode broek");
        Hint hPa5 = new Hint(ClothingType.Pants, ClothingColor.Yellow, "Heeft geen gele broek");

        Hint hSc1 = new Hint(ClothingType.Shoes, ClothingColor.Purple, "Heeft geen paarse schoenen");
        Hint hSc2 = new Hint(ClothingType.Shoes, ClothingColor.Green, "Heeft geen groene schoenen");
        Hint hSc3 = new Hint(ClothingType.Shoes, ClothingColor.Blue, "Heeft geen blauwe schoenen");
        Hint hSc4 = new Hint(ClothingType.Shoes, ClothingColor.Red, "Heeft geen rode schoenen");
        Hint hSc5 = new Hint(ClothingType.Shoes, ClothingColor.Yellow, "Heeft geen gele schoenen");

        hints.add(hHa1);
        hints.add(hHa2);
        hints.add(hHa3);
        hints.add(hHa4);
        hints.add(hHa5);

        hints.add(hSh1);
        hints.add(hSh2);
        hints.add(hSh3);
        hints.add(hSh4);
        hints.add(hSh5);

        hints.add(hPa1);
        hints.add(hPa2);
        hints.add(hPa3);
        hints.add(hPa4);
        hints.add(hPa5);

        hints.add(hSc1);
        hints.add(hSc2);
        hints.add(hSc3);
        hints.add(hSc4);
        hints.add(hSc5);
    }

    private static void fillNames() {
        names.clear();
        names.add("Klaas");
        names.add("Tinus");
        names.add("Jaap");
        names.add("Johan");
        names.add("Piet");
        names.add("Kees");
        names.add("Bart");
        names.add("Henk");
    }

    private static void filterHintsForWitch(Player w) {
        ArrayList<Hint> hintsToRemove = new ArrayList<>();
        for (Hint h : hints) {
            if (h.getClothing() == ClothingType.Hat && h.getClothColor() == w.getHatColor()) {
                hintsToRemove.add(h);
            } else if (h.getClothing() == ClothingType.Shirt && h.getClothColor() == w.getShirtColor()) {
                hintsToRemove.add(h);
            } else if (h.getClothing() == ClothingType.Pants && h.getClothColor() == w.getPantsColor()) {
                hintsToRemove.add(h);
            } else if (h.getClothing() == ClothingType.Shoes && h.getClothColor() == w.getShoesColor()) {
                hintsToRemove.add(h);
            }
        }
        hints.removeAll(hintsToRemove);
    }

    public static boolean giveHint(String ip, String rfid) {
        HintLoc loc = locations.get(rfid);
        if (loc != null
                ) {
            String hint = "Hint/ " + getHint(rfid) + "/" + rfid + "/" + (loc.isEnabled() ? "enabled" : "disabled");
            if (loc.isEnabled()) {
                loc.cooldown();
            }
            try {
                SocketController.sendMsgToPlayer(hint, connectedIps.get(ip));
                if (!hint.equals(HintLoc.getDisabledHintText())) {
                    return true;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            String hint = "Hint/ " + "Locatie niet gevonden" + "/" + rfid + "/" + "disabled";
            try {
                SocketController.sendMsgToPlayer(hint, connectedIps.get(ip));
                if (!hint.equals(HintLoc.getDisabledHintText())) {
                    return true;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    private static Hint getHint(ClothingType clothingType) {
        ArrayList<Hint> correctHints = new ArrayList<>();
        for (Hint h : hints) {
            if (h.getClothing() == clothingType) {
                correctHints.add(h);
            }
        }
        Random r = new Random();
        if (correctHints.size() > 0) {
            int randomInt = r.nextInt(correctHints.size());
            Hint h = correctHints.get(randomInt);
            hints.remove(h);
            noOfCorrectHintsFound = getNoOfCorrectHintsFound() + 1;
            return h;
        } else {
            Hint h = new Hint(null, null, HintLoc.getDisabledHintText());
            return h;
        }
    }

    public static boolean disableLocation(String rfid, Socket s) {
        Player p = connectedIps.get(s.getRemoteSocketAddress().toString());
        if (p.equals(witch) && !witchDisabledLoc) {
            HintLoc hl = locations.get(rfid);
            hl.setDisabled();
            witchDisabledLoc = true;
            try {
                SocketController.sendMsgToPlayer(hl.getName() + " disabled", p);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return true;
        } else if (!p.equals(witch)) {
            try {
                SocketController.sendMsgToPlayer("Error/Je bent niet de heks", p);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return false;
        } else if (witchDisabledLoc) {
            try {
                SocketController.sendMsgToPlayer("Error/Al een punt uitgeschakeld deze ronde", p);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return false;
        }
        return false;
    }

    public static int getMinNoPlayers() {
        return minNoPlayers;
    }

    public static int getCurrentRoundTime() {
        return currentRoundTime;
    }

    public static boolean isRoundActive() {
        return roundActive;
    }

    public static int getCurrentRound() {
        return currentRound;
    }

    public static int getNoOfCorrectHintsFound() {
        return noOfCorrectHintsFound;
    }

    public static ArrayList<Player> getPlayers() {
        return players;
    }

    public static void sendHeartbeat() throws IOException {
        Log.i("Heartbeat","Heartbeat sent");
        SocketController.sendMsgToAllPlayers("hb", getPlayers());
    }

    public static void respondHeartbeat(Socket socket){
        try {
            OutputStream outputStream = socket.getOutputStream();
            PrintStream printStream = new PrintStream(outputStream);
            printStream.println("hb_ok");
            printStream.flush();
            Log.i("Heartbeat", "Heartbeat sent to: " + socket.getRemoteSocketAddress().toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void heartbeatReceived(Context c, Socket s){
        Log.i("Heartbeat","Heartbeat received from: "+s.getRemoteSocketAddress().toString());
        if (connectedIps.containsKey(s.getRemoteSocketAddress().toString())) {
            ipHeartbeat.put(s.getRemoteSocketAddress().toString(), System.currentTimeMillis());
        }
    }

    public static void checkHeartbeat(Context c){
        /*for (Map.Entry<String, Long> entry : ipHeartbeat.entrySet()) {
            if (System.currentTimeMillis()-entry.getValue()> getHeartTimeout()){
                Player p = connectedIps.get(entry.getKey());
                //connectedIps.remove(entry.getKey());
               // players.remove(p);
                if (p!=null && witch !=null && p.equals(witch)){
                    Log.i("Heartbeat","Witch disconnected");
                }
                Intent intent = new Intent("GETDATA");
                intent.putExtra("DATA", GameController.getNoOfPlayers());
                c.sendBroadcast(intent);
            }
        }*/
    }

    public static long getHeartTimeout() {
        return heartTimeout;
    }
}

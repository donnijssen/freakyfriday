package fhict.bluetooth2.controllers;

import com.google.gson.Gson;

import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Random;

import fhict.bluetooth2.models.Json.CurrentPlayer;
import fhict.bluetooth2.models.Player;

/**
 * Created by Kai on 10-11-2016.
 */

public class PlayerController {
    private GameController gameController;
    private ArrayList<Player> players = new ArrayList<>();
    private ArrayList<String> names;
    private ArrayList<String> usedNames;
    private Player witch;

    public PlayerController(GameController gc){
        this.gameController = gc;
    }

    public boolean addPlayer(Socket socket){
        String ip = socket.getRemoteSocketAddress().toString();
        String ipNoPort = ip.split(":")[0];
        String ipToRemove = null;
        for (Player p: getPlayers()){
            String pNoPort = p.getIp().split(":")[0];
            if (pNoPort.equals(ipNoPort)){
                reconnectPlayer(socket);
            }
        }

        if (gameController.isGameStarted()) {
            return false;
        }
        Player p = getRandomPlayerConfig(socket);
        getPlayers().add(p);
        try {
            SocketController.sendMsgToPlayer("connected", p);
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean reconnectPlayer(Socket s){
        Player p = null;
        Player pToRemove = null;
        for (Player pl: getPlayers()){
            if (pl.getIpNoPort().equals(s.getRemoteSocketAddress().toString().split(":")[0])){
                p = pl;
                pToRemove = pl;
            }
        }
        if (p == null || s == null){
            return false;
        }
        p.setSocket(s);
        getPlayers().remove(pToRemove);
        getPlayers().add(p);
        if (gameController.isGameStarted()) {
            Gson gson = new Gson();
            String playersGson = gson.toJson(getPlayers());
            try {
                SocketController.sendMsgToPlayer("connected", p);
                SocketController.sendMsgToPlayer(playersGson, p);
                CurrentPlayer cp = new CurrentPlayer(p);
                String player = gson.toJson(cp);
                SocketController.sendMsgToPlayer(player, p);
                if (p.equals(getWitch())){
                    SocketController.sendMsgToPlayer("witch", p);
                }
                return true;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    public void setPlayerConfigs(){
        ArrayList<Player> newPlayers = new ArrayList<>();
        for (Player p : players) {
            Player p2 = getRandomPlayerConfig(p.getSocket());
            newPlayers.add(p2); // get New random players.
        }

        if (getWitch() == null || !gameController.isGameStarted()){
            Random r = new Random();
            int witchInt = r.nextInt(getPlayers().size());
            Player w = getPlayers().get(witchInt);
            gameController.getLocationController().filterHintsForWitch(w);
            witch = w;
        }
    }

    public boolean removePlayer(Socket s){
        if (gameController.isGameStarted()) {
            return false;
        }
        Player pToRemove = null;
        for (Player pl: getPlayers()){
            if (pl.getSocket().equals(s)){
                pToRemove = pl;
            }
        }
        getPlayers().remove(pToRemove);
        try {
            SocketController.sendMsgToPlayer("disconnected", pToRemove);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return true;
    }

    public Player getPlayerFromSocket(Socket s){
        for (Player p: getPlayers()){
            if (p.getSocket().equals(s)){
                return p;
            } else if (p.getIpNoPort().equals(s.getRemoteSocketAddress().toString().split(":")[0])){
                return p;
            }
        }
        return null;
    }

    public void fillWithData(){
        fillNames();
    }

    private void fillNames() {
        names =  new ArrayList<>();
        names.clear();
        names.add("Klaas");
        names.add("Tinus");
        names.add("Jaap");
        names.add("Johan");
        names.add("Piet");
        names.add("Kees");
        names.add("Bart");
        names.add("Henk");
    }

    public int getNoOfPlayers(){
        return players.size();
    }

    public Player getRandomPlayerConfig(Socket s){
        return null;
    }

    public ArrayList<Player> getPlayers() {
        return players;
    }

    public Player getWitch() {
        return witch;
    }
}

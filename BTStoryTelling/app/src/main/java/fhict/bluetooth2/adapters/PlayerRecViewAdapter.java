package fhict.bluetooth2.adapters;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import fhict.bluetooth2.models.Player;
import fhict.bluetooth2.R;

/**
 * Created by Kai on 14-10-2016.
 */

public class PlayerRecViewAdapter extends RecyclerView.Adapter<PlayerRecViewAdapter.PlayerViewHolder> {
    ArrayList<Player> players;
    Context mContext;

    Drawable dwHat;
    Drawable dwShirt;
    Drawable dwPants;
    Drawable dwShoes;

    public PlayerRecViewAdapter(Context c,ArrayList<Player> players){
        this.mContext = c;
        this.players = players;

        dwShirt = ContextCompat.getDrawable(mContext, R.drawable.shirt);
        dwPants = ContextCompat.getDrawable(mContext, R.drawable.pants);
        dwHat = ContextCompat.getDrawable(mContext, R.drawable.hat);
        dwShoes = ContextCompat.getDrawable(mContext, R.drawable.shoes);
    }

    @Override
    public PlayerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_player_item, parent, false);
        return new PlayerViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(PlayerViewHolder holder, int position) {
        Player p  = players.get(position);
        holder.tv_playerName.setText(p.getName());

        Drawable dwTop2 = dwShirt.getConstantState().newDrawable();
        Drawable dwPants2 = dwPants.getConstantState().newDrawable();
        Drawable dwHat2 = dwHat.getConstantState().newDrawable();
        Drawable dwShoes2 = dwShoes.getConstantState().newDrawable();
        dwTop2.mutate();
        dwPants2.mutate();
        dwHat2.mutate();
        dwShoes2.mutate();
        dwTop2.setColorFilter(p.getShirtAColor(), PorterDuff.Mode.SRC_ATOP);
        dwPants2.setColorFilter(p.getPantsAColor(), PorterDuff.Mode.SRC_ATOP);
        dwHat2.setColorFilter(p.getHatAColor(), PorterDuff.Mode.SRC_ATOP);
        dwShoes2.setColorFilter(p.getShoesAColor(), PorterDuff.Mode.SRC_ATOP);

        holder.iv_shirt.setImageDrawable(dwTop2);
        holder.iv_pants.setImageDrawable(dwPants2);
        holder.iv_hat.setImageDrawable(dwHat2);
        holder.iv_shoes.setImageDrawable(dwShoes2);

    }

    @Override
    public int getItemCount() {
        return players.size();
    }

    public class PlayerViewHolder extends RecyclerView.ViewHolder{

        public TextView tv_playerName;
        public ImageView iv_hat;
        public ImageView iv_shirt;
        public ImageView iv_pants;
        public ImageView iv_shoes;

        public PlayerViewHolder(View itemView) {
            super(itemView);
            tv_playerName = (TextView) itemView.findViewById(R.id.tv_playerName);
            iv_hat = (ImageView) itemView.findViewById(R.id.ivHat);
            iv_shirt = (ImageView) itemView.findViewById(R.id.ivTop);
            iv_pants = (ImageView) itemView.findViewById(R.id.ivBottom);
            iv_shoes = (ImageView) itemView.findViewById(R.id.ivShoe);

        }
    }
}

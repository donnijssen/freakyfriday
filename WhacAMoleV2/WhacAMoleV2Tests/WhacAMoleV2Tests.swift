//
//  WhacAMoleV2Tests.swift
//  WhacAMoleV2Tests
//
//  Created by Don Nijssen on 16-12-16.
//  Copyright © 2016 Don Nijssen. All rights reserved.
//

import XCTest
import Social
import SpriteKit
import GameplayKit

@testable import WhacAMoleV2

class WhacAMoleV2Tests: XCTestCase {
    var grass:SKSpriteNode!
    var mole:SKSpriteNode!
    var gameScene:GameScene!
    
    override func setUp() {
        super.setUp()
        grass = SKSpriteNode()
        grass.name = "Grass"
        
        mole = SKSpriteNode()
        mole.name = "Mole"
        
        if let scene = GKScene(fileNamed: "GameScene") {
            if let sceneNode = scene.rootNode as! GameScene? {
                gameScene = sceneNode
            }
        }
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testPerformanceExample() {
        self.measure {
        }
    }
    
    func testAtTweet(){
        if(SLComposeViewController.isAvailable(forServiceType: SLServiceTypeTwitter)) {
            // Create the tweet
            let tweet = SLComposeViewController(forServiceType: SLServiceTypeTwitter)
            tweet?.setInitialText("I just love the Whac a Mole game made by Don and Kai UNI TEST TWEET !!!  ")
            //GameViewController.present(tweet!, animated: true, completion: nil)
        }
    }

    func testiClickedAMoleTest(){
        
       let myscore = gameScene.iClickedAMole(node: mole)
        XCTAssertEqual(myscore, 10)
    }
    
    func testiDidntClickAMoleTest(){
        let myscore = gameScene.iClickedAMole(node: grass)
        XCTAssertEqual(myscore, 0)
    }
}

//
//  scoreCollectionViewCell.swift
//  WhacAMoleV2
//
//  Created by Fhict on 22/12/16.
//  Copyright © 2016 Don Nijssen. All rights reserved.
//

import UIKit

class scoreCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var lblName: UILabel!
    
    @IBOutlet weak var lblScore: UILabel!
}

//
//  Player.swift
//  WhacAMoleV2
//
//  Created by Fhict on 22/12/16.
//  Copyright © 2016 Don Nijssen. All rights reserved.
//

import Foundation

struct Player{
    var name: String
    var rating: Int
    
    init(name: String, rating: Int) {
        self.name = name
        self.rating = rating
    }
    
    func convertToDictionary() -> [String : Any] {
        let dic: [String: Any] = ["Name":self.name, "Rating":self.rating]
        return dic
    }
}

//
//  HighscoreCollectionViewController.swift
//  WhacAMoleV2
//
//  Created by Fhict on 22/12/16.
//  Copyright © 2016 Don Nijssen. All rights reserved.
//

import UIKit

class HighscoreCollectionViewController: UICollectionViewController {
    
    
    fileprivate let reuseIdentifier = "CellIdentifier"
    
    fileprivate let sectionInsets = UIEdgeInsets(top: 5.0, left: 0.0, bottom: 5.0, right: 0.0)
    
    fileprivate let itemsPerRow: CGFloat = 1
    
    fileprivate var highscorePlayers:[Player] = [
        Player(name:"Not changing", rating: 40),
        Player(name: "Example 2",  rating: 50),
        Player(name: "John Doe", rating: 20) ]
    
    /*override func viewDidLoad(){
        super.viewDidLoad()
        highscorePlayers = highscorePlayers.sorted(by: { $0.rating > $1.rating })
    }*/
    
    override func viewDidLoad() {
        let jsonHighscore = UserDefaults.standard.object(forKey: "highscorePlayers") as! String
        print("init")
        print(jsonHighscore)
        let dict = convertToDictionary(text: jsonHighscore)
        var highscore:[Player] = []
        
        for player in dict! {
            //let playerArray = player as! [String:String]
            var name:String = ""
            var rating:Int = 0
            
            for(key,value) in player{
                if (key)=="Name" {
                name = (value as! String)
                } else if (key)=="Rating" {
                    rating = (value as! Int)
                }
            }
            
            let player:Player = Player(name:name,rating:rating)
            highscore.append(player)
        }
        
       /* for (key, value) in dict! {
         
        }*/
        highscorePlayers = highscore
    }
    
    
    func convertToDictionary(text: String) -> [[String: Any]]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [[String: Any]]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
}
// MARK: - UICollectionViewDataSource
extension HighscoreCollectionViewController {
    //1
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    //2
    override func collectionView(_ collectionView: UICollectionView,
                                 numberOfItemsInSection section: Int) -> Int {
        return highscorePlayers.count
    }
    
    //3
    override func collectionView(_ collectionView: UICollectionView,
                                 cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        //1
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier,
                                                      for: indexPath) as! scoreCollectionViewCell
        //2
        let player = highscorePlayers[indexPath.row]
        cell.lblName.text=player.name
        cell.lblScore.text=String(player.rating)
        
        return cell
    }
}
extension HighscoreCollectionViewController : UICollectionViewDelegateFlowLayout {
    //1
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        //2
        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
        let availableWidth = view.frame.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow
        
        return CGSize(width: widthPerItem, height: 50)
    }
    
    //3
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    // 4
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInsets.left
    }
}

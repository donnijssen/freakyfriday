//
//  GameViewController.swift
//  WhacAMoleV2
//
//  Created by Don Nijssen on 16-12-16.
//  Copyright © 2016 Don Nijssen. All rights reserved.
//

import UIKit
import SpriteKit
import GameplayKit
import AVFoundation
import TwitterKit
import Social

/*extension SKNode {
    class func unarchiveFromFile(_ file : String) -> SKNode? {
        if let path = Bundle.main.path(forResource: file, ofType: "sks") {
            var sceneData = Data.(path, options: .DataReadingMappedIfSafe, error: nil)
            var archiver = NSKeyedUnarchiver(forReadingWithData: sceneData)
            
            archiver.setClass(self.classForKeyedUnarchiver(), forClassName: "SKScene")
            let scene = archiver.decodeObjectForKey(NSKeyedArchiveRootObjectKey) as GameScene
            archiver.finishDecoding()
            return scene
        } else {
            return nil
        }
    }
}*/

class GameViewController: UIViewController {
    
    var moles:NSMutableArray = []
    var moleTexture = "mole_1.png"
    let kMoleHoleOffset = 155.0
    var laughArray:NSMutableArray = []
    var hitArray:NSMutableArray = []
    var laughAnimation:SKAction!
    var hitAnimation:SKAction!
    var scoreLabel:SKLabelNode = SKLabelNode(fontNamed: "Chalkduster")
    var score:Int = 0
    var totalSpawns:Int = 0
    var gameOver:Bool = false
    var laughSound:SKAction!
    var hitSound:SKAction!
    var backgroundMusicPlayer = AVAudioPlayer()
    
    var gameScene:GameScene!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //twitterhandler
        NotificationCenter.default.addObserver(self, selector: "handleTwitter", name: NSNotification.Name(rawValue: "tweet"), object: nil)
        
        // Load 'GameScene.sks' as a GKScene. This provides gameplay related content
        // including entities and graphs.
        if let scene = GKScene(fileNamed: "GameScene") {
            
            // Get the SKScene from the loaded GKScene
            if let sceneNode = scene.rootNode as! GameScene? {
                gameScene = sceneNode
                
                // Copy gameplay related content over to the scene
                //sceneNode.entities = scene.moles
                //sceneNode.graphs = scene.graphs
                
                // Set the scale mode to scale to fit the window
                sceneNode.scaleMode = .aspectFill
                
                // Present the scene
                if let view = self.view as! SKView? {
                    view.presentScene(sceneNode)
                    
                    view.ignoresSiblingOrder = true
                    
                    view.showsFPS = true
                    view.showsNodeCount = true
                }
            }
        }
        
        let myNotification = Notification.Name(rawValue:"highscoreNotification")
        let nc = NotificationCenter.default
        nc.addObserver(forName:myNotification, object:nil, queue:nil, using:catchNotification)
    }

    override var shouldAutorotate: Bool {
        return true
    }

    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        /*if UIDevice.current.userInterfaceIdiom == .phone {
            return .allButUpsideDown
        } else {
            return .all
        }*/
        return UIInterfaceOrientationMask.landscape
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }
    func catchNotification(notification:Notification) -> Void {
                print("gotoHighscore")
        let highscoreViewControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "HighscoreView") as? HighscoreCollectionViewController
        //.instantiateViewControllerWithIdentifier("HighscoreViewController") as? HighscoreTableViewController
        //self.navigationController?.pushViewController(highscoreViewControllerObj!, animated: true)
        //let vc = HighscoreCollectionViewController()
       // let viewController = HighscoreCollectionViewController(collectionViewLayout: highscoreViewControllerObj)
        
        var highscorePlayers:[Player] = [
            Player(name:"Example", rating: 40),
            Player(name: "Example 2",  rating: 50),
            Player(name: "John Doe", rating: 20) ]
        
        highscorePlayers.sort(by: { $0.rating > $1.rating })
        let dicArray = highscorePlayers.map { $0.convertToDictionary() }

        let jsonString = JSONStringify(value: dicArray as AnyObject)
        
        print(jsonString)
        UserDefaults.standard.setValue(jsonString, forKey: "highscorePlayers")
        
        self.present(highscoreViewControllerObj!, animated: true, completion: nil)
        
        // Stop listening notification
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "highscoreNotification"), object: nil);
        
    }
    
    func handleTwitter(){
        makeTweet()
        /*let logInButton = TWTRLogInButton { (session, error) in
            if let unwrappedSession = session {
                let alert = UIAlertController(title: "Logged In",
                                              message: "User \(unwrappedSession.userName) has logged in",
                    preferredStyle: UIAlertControllerStyle.alert
                )
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                self.makeTweet()
            } else {
                NSLog("Login error: %@", error!.localizedDescription);
            }
        }
        
        // TODO: Change where the log in button is positioned in your view
        logInButton.center = self.view.center
        self.view.addSubview(logInButton)*/
    }
    
    func makeTweet(){
        let myscore = gameScene.score
        if(SLComposeViewController.isAvailable(forServiceType: SLServiceTypeTwitter)) {
            // Create the tweet
            let tweet = SLComposeViewController(forServiceType: SLServiceTypeTwitter)
            tweet?.setInitialText("I just love the Whac a Mole game made by Don and Kai!!! I had a score of: \(myscore) ")
            self.present(tweet!, animated: true, completion: nil)
        }
    }
    
    func JSONStringify(value: AnyObject,prettyPrinted:Bool = false) -> String{
        
        let options = prettyPrinted ? JSONSerialization.WritingOptions.prettyPrinted : JSONSerialization.WritingOptions(rawValue: 0)
        
        
        if JSONSerialization.isValidJSONObject(value) {
            
            do{
                let data = try JSONSerialization.data(withJSONObject: value, options: options)
                if let string = NSString(data: data, encoding: String.Encoding.utf8.rawValue) {
                    return string as String
                }
            }catch {
                
                print("error")
                //Access error here
            }
            
        }
        return ""
        
    }
  }

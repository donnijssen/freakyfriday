//
//  ViewNormalGameController.swift
//  WhacAMoleProject
//
//  Created by Don Nijssen on 08-12-16.
//  Copyright © 2016 Don Nijssen. All rights reserved.
//

import SpriteKit
import AVFoundation
import GameplayKit
import TwitterKit

class GameScene: SKScene {
    var moles:NSMutableArray = []
    var moleTexture = "mole_1.png"
    let kMoleHoleOffset = 155.0
    //var laughArray:NSMutableArray = []
    var laughArray:[SKTexture] = []
    //var hitArray:NSMutableArray = []
    var hitArray:[SKTexture] = []
    var laughAnimation:SKAction!
    var hitAnimation:SKAction!
    var scoreLabel:SKLabelNode = SKLabelNode(fontNamed: "MarkerFelt-Wide")
    var score:Int = 0
    var totalSpawns:Int = 0
    var gameOver:Bool = false
    var laughSound:SKAction!
    var hitSound:SKAction!
    var backgroundMusicPlayer = AVAudioPlayer()
    
    override func didMove(to view: SKView) {
        
        /* Setup your scene here */
        let Dirt:SKSpriteNode = SKSpriteNode(imageNamed: "Dirt")
        Dirt.position = CGPoint(x: self.frame.midX, y: self.frame.midY)
        Dirt.name = "Dirt"
        Dirt.xScale = 2.0
        Dirt.yScale = 2.0
        self.addChild(Dirt)
        
        let Lower:SKSpriteNode = SKSpriteNode(imageNamed: "GrassLow")
        Lower.anchorPoint = CGPoint(x: 0.5, y: 1.0);
        Lower.name = "Lower"
        Lower.zPosition = 999
        Lower.position = CGPoint(x: self.frame.midX, y: self.frame.midY);
        
        let Upper:SKSpriteNode = SKSpriteNode(imageNamed: "GrassHigh")
        Upper.anchorPoint = CGPoint(x: 0.5, y: 0.0);
        Upper.name = "Upper"
        Upper.position = CGPoint(x: self.frame.midX, y: self.frame.midY)
        
        self.addChild(Upper)
        
        //_:Float = 240.0
        
        let mole1:SKSpriteNode = SKSpriteNode(imageNamed: "Mole")
        mole1.anchorPoint = CGPoint(x: 0.8, y: 0.0)
        mole1.position = CGPoint(x: (self.frame.midX / 2), y: 130)
        mole1.name = "Mole"
        mole1.userData = NSMutableDictionary()
        self.addChild(mole1)
        moles.add(mole1)
        
        let mole2:SKSpriteNode = SKSpriteNode(imageNamed: "Mole")
        mole2.anchorPoint = CGPoint(x: 0.5, y: 0.0)
        mole2.position = CGPoint(x: (self.frame.midX), y: 130)
        mole2.name = "Mole"
        mole2.userData = NSMutableDictionary()
        self.addChild(mole2)
        moles.add(mole2)
        
        let mole3:SKSpriteNode = SKSpriteNode(imageNamed: "Mole")
        mole3.anchorPoint = CGPoint(x: 0.9, y: 0.0)
        mole3.position = CGPoint(x: (self.frame.maxX-(self.frame.midX/4)), y: 130)
        mole3.name = "Mole"
        mole3.userData = NSMutableDictionary()
        self.addChild(mole3)
        moles.add(mole3)
        
        self.addChild(Lower)
        for index in 1...3 {
            print("Laugh\(index)")
            laughArray.append(SKTexture(imageNamed: "Laugh\(index)"))
        }
        
        laughAnimation =  SKAction.animate(with: laughArray, timePerFrame: 0.2)
        
        for index in 1...4 {
            print("Hit\(index)")
            //itArray.add(SKTexture(imageNamed: "Hit\(index)"))
            hitArray.append(SKTexture(imageNamed: "Hit\(index)"))
        }
        
        hitAnimation = SKAction.animate(with: hitArray, timePerFrame: 0.2)
        
        scoreLabel.text = "Score : 0"
        scoreLabel.name = "scoreLabel"
        scoreLabel.fontSize = 60
        scoreLabel.zPosition = 999999
        scoreLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.center
        scoreLabel.position = CGPoint(x: self.frame.midX, y: self.frame.midY + (scoreLabel.frame.height * 4))
        self.addChild(scoreLabel)
        
        laughSound = SKAction.playSoundFileNamed("laugh.caf", waitForCompletion: false)
        hitSound = SKAction.playSoundFileNamed("ow.caf", waitForCompletion: false)
        
        /*do{
            var musicURL = Bundle.main.url(forResource: "whack", withExtension: "caf")
            var error : NSError? = nil
            backgroundMusicPlayer = try AVAudioPlayer(contentsOf: musicURL!)
            backgroundMusicPlayer.numberOfLoops = -1
            backgroundMusicPlayer.play()
        }catch{
            print("audio not working!")
        }*/
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        /* Called when a touch begins */
        
        for touch: AnyObject in touches {
            let location = touch.location(in: self)
            var node:SKNode = atPoint(location)
            if (node.name == "Mole") {
                var mole:SKSpriteNode = node as! SKSpriteNode
                score = iClickedAMole(node: node)
                mole.userData?.setObject(0, forKey: "tappable" as NSCopying)
                mole.removeAllActions()
                var easeMoveDown:SKAction = SKAction.moveTo(y: mole.position.y - mole.size.height, duration: 0.2)
                easeMoveDown.timingMode = SKActionTimingMode.easeOut
                
                //var sequence:SKAction = SKAction.sequence([ easeMoveDown])
                var sequence:SKAction = SKAction.sequence([hitSound, hitAnimation, easeMoveDown])
                mole.run(sequence)
            } else if (node.name == "Highscore"){
                print("HIGHSCORE")
                //let vc = HighscoreViewController.instantiate()
                //self.present(vc, animated: true, completion: nil)
                
                let myNotification = Notification.Name(rawValue:"highscoreNotification")
                let nc = NotificationCenter.default
                nc.post(name:myNotification,
                        object: nil,
                        userInfo:["message":"Hello there!", "date":Date()])

                
            } else if(node.name == "tryagain"){
                //SKView.presentScene("GameScene")
                let gameScene:GameScene = GameScene(size: self.view!.bounds.size) 
                let transition = SKTransition.fade(withDuration: 1.0)
                gameScene.scaleMode = SKSceneScaleMode.fill
                self.view!.presentScene(gameScene, transition: transition)
                
            } else if(node.name == "logInButton" || node.name == "twiterLabel"){
                 gameOvetTweet()
            
            }else{
                return
            }
        }
    }
    
    override func update(_ currentTime: TimeInterval) {
        /* Called before each frame is rendered */
        
        if gameOver == true {return}
        
        if totalSpawns >= 50 {
            gameOver =  true
            let gameOverLabel = SKLabelNode(fontNamed: "MarkerFelt-Wide")
            gameOverLabel.text = "Level Completed"
            gameOverLabel.name = "scoreLabel"
            gameOverLabel.fontSize = 70
            gameOverLabel.zPosition = 99999999
            gameOverLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.center
            gameOverLabel.position = CGPoint(x: self.frame.midX, y: self.frame.midY)
            self.addChild(gameOverLabel)
            
            let tryAgainClickLabel = SKLabelNode(fontNamed: "ArrailBoldMT")
            tryAgainClickLabel.text = "Try Again?"
            tryAgainClickLabel.name = "tryagain"
            tryAgainClickLabel.fontSize = 50
            tryAgainClickLabel.zPosition = 99999999
            tryAgainClickLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.center
            tryAgainClickLabel.position = CGPoint(x: self.frame.midX, y: self.frame.midY-60)
            self.addChild(tryAgainClickLabel)
            
            let highscoreLabel = SKLabelNode(fontNamed: "Arial-BoldMT")
            highscoreLabel.text = "Highscore: 90"
            highscoreLabel.name = "Highscore"
            highscoreLabel.fontSize = 70
            highscoreLabel.zPosition = 99999999
            highscoreLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.center
            highscoreLabel.position = CGPoint(x: self.frame.midX, y: gameOverLabel.frame.minY-(scoreLabel.frame.height * 4))
            self.addChild(highscoreLabel)
            
            let highscoreClickLabel = SKLabelNode(fontNamed: "Arial-BoldMT")
            highscoreClickLabel.text = "Click for Highscores"
            highscoreClickLabel.name = "Click for Highscores"
            highscoreClickLabel.fontSize = 60;
            highscoreClickLabel.zPosition = 99999999
            highscoreClickLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.center
            highscoreClickLabel.position = CGPoint(x: self.frame.midX, y: highscoreLabel.frame.minY-50)
            self.addChild(highscoreClickLabel)
            
            let logInButton = SKSpriteNode(color: SKColor.blue, size: CGSize(width: 250, height: 54))
            logInButton.name = "logInButton"
            logInButton.zPosition = 99999999
            logInButton.position = CGPoint(x: self.frame.midX, y: highscoreLabel.frame.minY-120)
            self.addChild(logInButton)
            
            let postTitle = SKLabelNode(fontNamed: "MarkerFelt-Wide")
            postTitle.text = "Post Score!"
            postTitle.name = "twiterLabel"
            postTitle.fontSize = 28
            postTitle.zPosition = 99999999
            postTitle.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.center
            postTitle.position = CGPoint(x: self.frame.midX, y: self.frame.midY-360)
            self.addChild(postTitle)
        
        }
        
        for mole in moles {
            //println()
            if (arc4random() % 5 == 0) {
                //println(mole.position.y)
                if ((mole as AnyObject).position.y == 130.0 as CGFloat) {
                    popMole(mole as! SKSpriteNode)
                }
            }
        }
    }
    
    func popMole(_ mole:SKSpriteNode) {
        if totalSpawns > 50 {return}
        totalSpawns += 1
        mole.texture = SKTexture(imageNamed: "Mole")
        
        let easeMoveUp:SKAction = SKAction.moveTo(y: mole.position.y + mole.size.height, duration: 0.2)
        easeMoveUp.timingMode = SKActionTimingMode.easeOut
        let easeMoveDown:SKAction = SKAction.moveTo(y: mole.position.y, duration: 0.2)
        easeMoveDown.timingMode = SKActionTimingMode.easeOut
        
        let setTappable:SKAction = SKAction.run { () -> Void in
            mole.userData?.setObject(1, forKey: "tappable" as NSCopying)
            return Void()
        }
        
        let unsetTappable:SKAction = SKAction.run { () -> Void in
            mole.userData?.setObject(0, forKey: "tappable" as NSCopying)
            return Void()
        }
        
        //var delay:SKAction = SKAction.waitForDuration(0.5)
        let sequence:SKAction = SKAction.sequence([easeMoveUp, setTappable, laughAnimation ,unsetTappable, easeMoveDown])
        mole.run(sequence)
    }
    
    func iClickedAMole(node:SKNode) -> Int{
        var inscreasescore:Int = score
        if (node.name == "Mole") {
            inscreasescore += 10
            scoreLabel.text = "Score : \(score)"
        }
        return inscreasescore
        
    }
    
    //twitter handler
    func gameOvetTweet() {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "tweet"), object: nil)
    }
}

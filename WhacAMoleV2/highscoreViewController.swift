//
//  highscoreViewController.swift
//  WhacAMoleV2
//
//  Created by Fhict on 20/12/16.
//  Copyright © 2016 Don Nijssen. All rights reserved.
//

import UIKit
import SpriteKit
import GameplayKit

class highscoreViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Load 'HighscoreScene.sks' as a GKScene.
        if let scene = GKScene(fileNamed: "HighscoreScene") {
            
            // Get the SKScene from the loaded GKScene
            if let sceneNode = scene.rootNode as! HighscoreScene? {
                
                // Copy gameplay related content over to the scene
                //sceneNode.entities = scene.moles
                //sceneNode.graphs = scene.graphs
                
                // Set the scale mode to scale to fit the window
                sceneNode.scaleMode = .aspectFill
                
                // Present the scene
                if let view = self.view as! SKView? {
                    view.presentScene(sceneNode)
                    
                    view.ignoresSiblingOrder = true
                    
                    view.showsFPS = true
                    view.showsNodeCount = true
                }
            }
        }


        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

﻿using UnityEngine;
using System.Collections;

public class GMClient : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void PlaySound(int _deviceID)
    {
        //Do stuff.
        Debug.Log("Playing sound on device " + _deviceID);
    }

    public void LightDevice(int _deviceID)
    {
        //Do stuff.
        Debug.Log("Lighting device " + _deviceID);
    }

    public void VibrateDevice(int _deviceID)
    {
        //Do stuff.
        Debug.Log("Vibrating device " + _deviceID);
    }
}

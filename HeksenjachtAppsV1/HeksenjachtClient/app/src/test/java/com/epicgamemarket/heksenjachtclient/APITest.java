package com.epicgamemarket.heksenjachtclient;


import com.epicgamemarket.heksenjachtclient.Network.APIConnector;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import java.util.HashMap;

import static org.junit.Assert.*;
/**
 * Created by dnijs on 22/11/2016.
 */

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class APITest {

    private APIConnector apiConnector;
    //api url
    private String url ="http://heksenjacht.codesolutions.nl/Functions/loginUser.php?";
    private String urlRegister ="http://heksenjacht.codesolutions.nl/Functions/registerUsers.php?";
    private String urlCreateGame ="http://heksenjacht.codesolutions.nl/Functions/createGame.php?";

    @Before
    public void APITESTSetup()
    {
       apiConnector = new APIConnector();
    }

    @Test
    public void userLogin(){
        HashMap<String, String> data = new HashMap<String,String>();
        data.put("username","test");
        data.put("password","test");

        String result = apiConnector.sendPostRequest(url,data);

        assertEquals("successfully logged in", result);
    }

    @Test
    public void wrongUserLogin(){
        HashMap<String, String> data = new HashMap<String,String>();
        data.put("username","testx");
        data.put("password","testx");

        String result = apiConnector.sendPostRequest(url,data);


        assertEquals("oops! Please try again! ", result);
    }

    @Test
    public void registerUser(){
        HashMap<String, String> data = new HashMap<String,String>();
        data.put("username","test2");
        data.put("password","test2");
        data.put("password","testuser2");

        String result = apiConnector.sendPostRequest(urlRegister,data);

        assertEquals("successfully created", result);
    }

    @Test
    public void wrongRegisterUser(){
        HashMap<String, String> data = new HashMap<String,String>();
        data.put("username","test2");
        data.put("password","test2");
        data.put("password","testuser2");

        String result = apiConnector.sendPostRequest(urlRegister,data);

        assertEquals("successfully created", result);
    }

    @Test
    public void createGame(){
        HashMap<String, String> data = new HashMap<String,String>();
        data.put("gamename","test2");

        String result = apiConnector.sendPostRequest(urlCreateGame,data);

        assertEquals("successfully created", result);
    }

    @Test
    public void wrongCreateGame(){
        HashMap<String, String> data = new HashMap<String,String>();
        data.put("gamename","test2");

        String result = apiConnector.sendPostRequest(urlCreateGame,data);

        assertEquals("oops! Please try again! ", result);
    }

}

package com.epicgamemarket.heksenjachtclient.Domain;

/**
 * Created by Kai on 6-10-2016.
 */

public class HintLoc {
    private String rfid;
    private String name;
    private ClothingType type;
    private boolean enabled = true;
    private static String disabledHintText = "Er zijn geen hints beschikbaar.";

    public HintLoc(String rfid, String name, ClothingType type){
        this.rfid = rfid;
        this.name = name;
        this.type = type;
    }

    public static String getDisabledHintText() {
        return disabledHintText;
    }

    public void setEnabled(){
        enabled = true;
    }

    public void setDisabled(){
        enabled  = false;
    }

    public String getRfid() {
        return rfid;
    }

    public String getName() {
        return name;
    }

    public ClothingType getType() {
        return type;
    }

    public boolean isEnabled() {
        return enabled;
    }
}

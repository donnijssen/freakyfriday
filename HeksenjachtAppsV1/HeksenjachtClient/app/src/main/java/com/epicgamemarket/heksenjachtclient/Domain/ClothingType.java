package com.epicgamemarket.heksenjachtclient.Domain;

/**
 * Created by Kai on 6-10-2016.
 */

public enum ClothingType {
    Hat, Shirt, Pants, Shoes
}

package com.epicgamemarket.heksenjachtclient.Domain;

import java.net.Socket;

/**
 * Created by dnijs on 10/10/2016.
 */

public class Server {

    private transient Socket socket;
    public static final int SERVERPORT = 8000;

    public Server(Socket socket){
        this.socket = socket;
    }

    public int getServerPort(){
        return SERVERPORT;
    }

    public Socket getSocket() {
        return socket;
    }

}

package com.epicgamemarket.heksenjachtclient.Network;


import com.epicgamemarket.heksenjachtclient.Domain.Server;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;

/**
 * Created by dnijs on 10/10/2016.
 */

public class SocketController {
    public static void sendMsgToServer(String msg,Server s) throws IOException {
        OutputStream outputStream = s.getSocket().getOutputStream();
        PrintStream printStream = new PrintStream(outputStream);
        printStream.print(msg);
        //printStream.close();
    }
}

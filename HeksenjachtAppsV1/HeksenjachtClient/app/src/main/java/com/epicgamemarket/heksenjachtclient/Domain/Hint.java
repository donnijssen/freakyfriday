package com.epicgamemarket.heksenjachtclient.Domain;

/**
 * Created by Kai on 6-10-2016.
 */

public class Hint {
    private ClothingType clothing;
    private ClothingColor clothColor;
    private String hint;
    private boolean shown;
    private String rfid;
    private boolean enabled;

    public Hint(ClothingType ht, ClothingColor c,String h){
        this.clothing  =ht;
        this.clothColor = c;
        this.hint = h;
    }

    public Hint(String hint, String rfid, boolean enabled){
        this.hint = hint;
        this.rfid = rfid;
        this.enabled  =enabled;
    }

    public ClothingType getClothing() {
        return clothing;
    }

    public ClothingColor getClothColor() {
        return clothColor;
    }

    public String getHint() {
        return hint;
    }

    public boolean isShown() {
        return shown;
    }

    public void setShown() {
        this.shown = true;
    }

    public String getRfid() {
        return rfid;
    }

    public boolean isEnabled() {
        return enabled;
    }
}

package com.epicgamemarket.heksenjachtclient.Domain.Json;


import com.epicgamemarket.heksenjachtclient.Domain.Player;

import java.util.ArrayList;

/**
 * Created by Kai on 10-10-2016.
 */

public class Players {
    private ArrayList<Player> players;

    public Players(ArrayList<Player> players){
        this.players = players;
    }

    public ArrayList<Player> getPlayers(){
        return players;
    }
}

package com.epicgamemarket.heksenjachtclient.Domain;

/**
 * Created by dnijs on 06/10/2016.
 */

public class Character {

    private String name;
    private String gender;
    private String topType;
    private String bottomType;
    private String hairColor;
    private String topColor;
    private String bottomColor;
    private String shoeColor;
    private String hatColor;
    private boolean isWitch;

    public Character(String name, String gender, String topType, String bottomType, String hairColor, String topColor, String bottomColor, String shoeColor, String hatColor, boolean isWitch) {
        this.name = name;
        this.gender = gender;
        this.topType = topType;
        this.bottomType = bottomType;
        this.hairColor = hairColor;
        this.topColor = topColor;
        this.bottomColor = bottomColor;
        this.shoeColor = shoeColor;
        this.hatColor = hatColor;
        this.isWitch = isWitch;
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getTopType() {
        return topType;
    }

    public void setTopType(String topType) {
        this.topType = topType;
    }

    public String getBottomType() {
        return bottomType;
    }

    public void setBottomType(String bottomType) {
        this.bottomType = bottomType;
    }

    public String getHairColor() {
        return hairColor;
    }

    public void setHairColor(String hairColor) {
        this.hairColor = hairColor;
    }

    public String getTopColor() {
        return topColor;
    }

    public void setTopColor(String topColor) {
        this.topColor = topColor;
    }

    public String getBottomColor() {
        return bottomColor;
    }

    public void setBottomColor(String bottomColor) {
        this.bottomColor = bottomColor;
    }

    public String getShoeColor() {
        return shoeColor;
    }

    public void setShoeColor(String shoeColor) {
        this.shoeColor = shoeColor;
    }

    public String getHatColor() {
        return hatColor;
    }

    public void setHatColor(String hatColor) {
        this.hatColor = hatColor;
    }

    public boolean isWitch() {
        return isWitch;
    }

    public void setWitch(boolean witch) {
        isWitch = witch;
    }





}

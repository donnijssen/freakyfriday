package com.epicgamemarket.heksenjachtclient;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.epicgamemarket.heksenjachtclient.Network.APIConnector;

import java.util.HashMap;

public class LoginActivity extends AppCompatActivity {

    //controls
    private TextView tvRegister;
    private EditText etUsername;
    private EditText etPassword;
    private Button btnLogin;

    //api url
    private String url ="http://heksenjacht.codesolutions.nl/Functions/loginUser.php?";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        etUsername = (EditText)findViewById(R.id.etUsername);
        etPassword = (EditText)findViewById(R.id.etPassword);

        tvRegister = (TextView)findViewById(R.id.tvRegister);
        tvRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(intent);
            }
        });

        btnLogin = (Button)findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginUser();
            }
        });
    }

    private void loginUser() {
        String username = etUsername.getText().toString();
        String password = etPassword.getText().toString();
        login(username, password);
    }

    private void login(String username,String password) {
        class LoginUser extends AsyncTask<String, Void, String> {
            ProgressDialog loading;
            APIConnector apiConnector = new APIConnector();

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(LoginActivity.this, "Please Wait",null, true, true);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                if(s.equalsIgnoreCase("successfully logged in"))
                {
                    //SharedPreferences.Editor editor = sharedpreferences.edit();
                    //editor.putString("user", myUser );
                    //editor.commit();
                    Intent intent = new Intent(LoginActivity.this, GameActivity.class);
                    Toast.makeText(getApplicationContext(),"yeAHHHH", Toast.LENGTH_LONG).show();
                    startActivity(intent);
                }
            }

            @Override
            protected String doInBackground(String... params) {

                HashMap<String, String> data = new HashMap<String,String>();
                data.put("username",params[0]);
                data.put("password",params[1]);

                String result = apiConnector.sendPostRequest(url,data);

                if(result.equalsIgnoreCase("successfully logged in"))
                {
                    //SharedPreferences.Editor editor = sharedpreferences.edit();
                    //editor.putString("user", myUser );
                    //editor.commit();

                }
                return  result;
            }
        }

        LoginUser loginUser = new LoginUser();
        loginUser.execute(username, password);
    }
}

package com.epicgamemarket.heksenjachtclient;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.epicgamemarket.heksenjachtclient.Network.APIConnector;

import java.util.HashMap;

public class GameActivity extends AppCompatActivity {
    //controls
    private EditText etGamename;
    private Button btnCreateGame;

    //api url
    private String url ="http://heksenjacht.codesolutions.nl/Functions/createGame.php?";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        etGamename = (EditText)findViewById(R.id.etGamename);

        btnCreateGame = (Button)findViewById(R.id.btnCreateGame);
        btnCreateGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createGame();
            }
        });
    }

    public void createGame(){
        String gamename = etGamename.getText().toString();
        createGameTask(gamename);
    }

    private void createGameTask(String gamename){
        class GameTask extends AsyncTask<String, Void, String> {
            ProgressDialog loading;
            APIConnector apiConnector = new APIConnector();

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(GameActivity.this, "Please Wait",null, true, true);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                if(s.equalsIgnoreCase("successfully created"))
                {
                    //SharedPreferences.Editor editor = sharedpreferences.edit();
                    //editor.putString("user", myUser );
                    //editor.commit();
                    Intent intent = new Intent(GameActivity.this, MainActivity.class);
                    Toast.makeText(getApplicationContext(),"Game created!", Toast.LENGTH_LONG)
                            .show();
                    startActivity(intent);
                }
            }

            @Override
            protected String doInBackground(String... params) {

                HashMap<String, String> data = new HashMap<String,String>();
                data.put("gamename",params[0]);

                String result = apiConnector.sendPostRequest(url,data);

                if(result.equalsIgnoreCase("successfully created"))
                {
                    //SharedPreferences.Editor editor = sharedpreferences.edit();
                    //editor.putString("user", myUser );
                    //editor.commit();

                }
                return  result;
            }
        }

        GameTask gameTask = new GameTask();
        gameTask.execute(gamename);
    }
}

package com.epicgamemarket.heksenjachtclient;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.epicgamemarket.heksenjachtclient.Controllers.GameController;
import com.epicgamemarket.heksenjachtclient.Network.APIConnector;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class RegisterActivity extends AppCompatActivity {

    //controls
    private Button btnRegister;
    private EditText etUsername;
    private EditText etPassword;
    private EditText etRepassword;
    private EditText etDisplayname;

    //api url
    private String url ="http://heksenjacht.codesolutions.nl/Functions/registerUsers.php?";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        etUsername = (EditText)findViewById(R.id.etUsername);
        etPassword = (EditText)findViewById(R.id.etPassword);
        etRepassword = (EditText)findViewById(R.id.etRePassword);
        etDisplayname =(EditText)findViewById(R.id.etDisplayname);

        btnRegister = (Button)findViewById(R.id.btnReg);
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkInput();
            }
        });
    }

    private void checkInput(){
        String username = etUsername.getText().toString();
        String password = etPassword.getText().toString();
        String repassword = etRepassword.getText().toString();
        String displayname = etDisplayname.getText().toString();

        //checksss
        if(!username.isEmpty() && !displayname.isEmpty() && !username.contentEquals(displayname)){
            if(password.contentEquals(repassword)){
                registerUser(username,password,displayname);
            }else{
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        RegisterActivity.this);
                alertDialogBuilder
                        .setCancelable(false)
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        });

                alertDialogBuilder.setMessage("Fout wachtwoord!");

                alertDialogBuilder.create().show();
            }
        }else{
            //error
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                    RegisterActivity.this);
            alertDialogBuilder
                    .setCancelable(false)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                        }
                    });

            alertDialogBuilder.setMessage("Fout display en username");

            alertDialogBuilder.create().show();
        }
    }


    private void registerUser(String username,String password,String displayname){
        class RegisterUser extends AsyncTask<String, Void, String> {
            ProgressDialog loading;
            APIConnector apiConnector = new APIConnector();

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(RegisterActivity.this, "Please Wait",null, true, true);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();

            }

            @Override
            protected String doInBackground(String... params) {

                HashMap<String, String> data = new HashMap<String,String>();
                data.put("username",params[0]);
                data.put("password",params[1]);
                data.put("displayname",params[2]);

                String result = apiConnector.sendPostRequest(url,data);
                if(result.equalsIgnoreCase("successfully created"))
                {
                    Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                    startActivity(intent);
                }

                return  result;
            }
        }

        RegisterUser registerUser = new RegisterUser();
        registerUser.execute(username, password, displayname);
    }

/*
    public void registerUser(final String username, final String password, final String displayname)
    {
        String url = "http://heksenjacht.codesolutions.nl/Functions/registerUsers.php";

        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            System.out.print(response);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<>();
                // the POST parameters:
                params.put("username", username);
                params.put("password", password);
                params.put("displayname", displayname);
                return params;
            }

        };
        Volley.newRequestQueue(this).add(postRequest);
    }
    */
}

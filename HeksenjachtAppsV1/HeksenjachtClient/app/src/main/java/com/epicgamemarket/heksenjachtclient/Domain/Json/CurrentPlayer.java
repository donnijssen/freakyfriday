package com.epicgamemarket.heksenjachtclient.Domain.Json;


import com.epicgamemarket.heksenjachtclient.Domain.Player;

/**
 * Created by Kai on 11-10-2016.
 */

public class CurrentPlayer {

    private Player currentPlayer;

    public CurrentPlayer(Player p){
        this.currentPlayer = p;
    }

    public Player getCurrentPlayer(){
        return currentPlayer;
    }
}

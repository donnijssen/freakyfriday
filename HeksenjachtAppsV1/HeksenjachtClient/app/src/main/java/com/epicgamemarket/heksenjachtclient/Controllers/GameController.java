package com.epicgamemarket.heksenjachtclient.Controllers;
import com.epicgamemarket.heksenjachtclient.Domain.Player;

import java.util.ArrayList;

/**
 * Created by Kai on 11-10-2016.
 */

public class GameController {
    //hoi kai
    private static ArrayList<Player> players;
    private static Player currentPlayer;
    private static boolean isWitch = false;
    private static long lastHeartbeat = -1;
    private static long heartTimeout = 5* 1000;

    private static boolean gameStarted = false;
    private static boolean connected = false;
    private static boolean roundActive = false;


    public static ArrayList<Player> getPlayers() {
        return players;
    }

    public static Player getCurrentPlayer() {
        return currentPlayer;
    }

    public static boolean isWitch() {
        return isWitch;
    }

    public static void setWitch(){
        isWitch = true;
    }

    public static boolean isGameStarted() {
        return gameStarted;
    }

    public static void setGameStarted(boolean gameStarted) {
        GameController.gameStarted = gameStarted;
    }

    public static boolean isConnected() {
        return connected;
    }

    public static void setConnected(boolean connected) {
        GameController.connected = connected;
    }

    public static boolean isRoundActive() {
        return roundActive;
    }

    public static void setRoundActive(boolean roundActive) {
        GameController.roundActive = roundActive;
    }

    public static void setPlayers(ArrayList<Player> players) {
        GameController.players = players;
    }

    public static void setCurrentPlayer(Player currentPlayer) {
        GameController.currentPlayer = currentPlayer;
    }

    public static void endGame(){
        isWitch= false;
        gameStarted = false;
        roundActive = false;
        players.clear();
        currentPlayer = null;
    }

}

package com.epicgamemarket.heksenjachtserver;

import com.epicgamemarket.heksenjachtserver.Controllers.GameController;
import com.epicgamemarket.heksenjachtserver.Domain.ClothingColor;
import com.epicgamemarket.heksenjachtserver.Domain.Hint;
import com.epicgamemarket.heksenjachtserver.Domain.Player;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import java.io.IOException;
import java.net.Socket;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class GameControllerUnitTest {
    private static GameController gameController = new GameController();

    public GameControllerUnitTest(){
        super();
    }

    @Test
    public void addition_isCorrect() throws Exception {
        assertEquals(4, 2 + 2);
    }

    @Test
    public void a1addPlayers() throws Exception{
        gameController.fillGameData();
        gameController.getPlayerController().addPlayer(new Socket("localhost",80));
        int noOfPlayers = gameController.getPlayerController().getNoOfPlayers();
        assertEquals(1,noOfPlayers);
    }

    @Test
    public void a2startGame() {
        gameController.startGame();
        assertTrue(gameController.isGameStarted());
        assertNotNull(gameController.getPlayerController().getWitch());
    }

    @Test
    public void a3startRound() throws InterruptedException {
        gameController.startRound(1);
        assertTrue(gameController.isRoundActive());
    }

    @Test
    public void a4getHint() throws IOException {
        boolean hint = gameController.getLocationController().giveHint(new Socket("localhost",80),"04916072952B80");
        assertTrue(hint);
    }

    @Test
    public void a5disableLoc() throws IOException{
        gameController.getLocationController().disableLocation("04916072952B80",new Socket("localhost",80));
        boolean hint = gameController.getLocationController().giveHint(new Socket("localhost",80),"04916072952B80");
        assertFalse(hint);
    }

    @Test
    public void a6stopGame(){
        gameController.stopGame();
        assertFalse(gameController.isRoundActive());
        assertFalse(gameController.isGameStarted());
    }
}
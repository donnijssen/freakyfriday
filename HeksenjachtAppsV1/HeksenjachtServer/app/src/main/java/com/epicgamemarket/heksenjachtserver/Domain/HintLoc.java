package com.epicgamemarket.heksenjachtserver.Domain;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.epicgamemarket.heksenjachtserver.Controllers.GameController;

import java.io.Serializable;


/**
 * Created by Kai on 6-10-2016.
 */

public class HintLoc implements Serializable{
    private String rfid;
    private String name;
    private ClothingType type;
    private boolean enabled = true;
    private boolean cooldown = false;
    private static String disabledHintText = "Je krijgt nog geen hint!";

    public HintLoc(String rfid, String name, ClothingType type){
        this.rfid = rfid;
        this.name = name;
        this.type = type;
    }

    public static String getDisabledHintText() {
        return disabledHintText;
    }

    public void setEnabled(){
        enabled = true;
    }

    public void setDisabled(){
        enabled  = false;
    }

    public String getRfid() {
        return rfid;
    }

    public String getName() {
        return name;
    }

    public ClothingType getType() {
        return type;
    }

    public boolean isEnabled() {
        return enabled && !cooldown;
    }

    public void cooldown(){
        cooldown  = true;
       /* if (Looper.myLooper() == null) {
            Looper.prepare();
        }*/
        Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(new Runnable() {

            @Override
            public void run() {
                cooldown = false;
                Log.i("HL","Hintloc cooldown disabled");
            }

        },  10000);
        // Looper.loop();
    }
}

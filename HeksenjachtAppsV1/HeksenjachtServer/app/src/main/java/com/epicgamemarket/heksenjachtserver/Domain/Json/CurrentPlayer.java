package com.epicgamemarket.heksenjachtserver.Domain.Json;


import com.epicgamemarket.heksenjachtserver.Domain.Player;

/**
 * Created by Kai on 11-10-2016.
 */

public class CurrentPlayer {

    private Player currentPlayer;

    public CurrentPlayer(Player p){
        this.currentPlayer = p;
    }
}

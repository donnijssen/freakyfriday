package com.epicgamemarket.heksenjachtserver.Domain;

import java.io.Serializable;

/**
 * Created by Kai on 6-10-2016.
 */

public enum ClothingColor  implements Serializable {
    Red,Green,Yellow,Purple,Blue
}

package com.epicgamemarket.heksenjachtserver.Adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.epicgamemarket.heksenjachtserver.R;

import java.util.ArrayList;

/**
 * Created by Kai on 14-10-2016.
 */

public class FragmentPagerAdapter extends android.support.v4.app.FragmentPagerAdapter {

    ArrayList<Fragment> fragments = new ArrayList<>();
    String[] tabTitles = new String[]{"Controls","Statistics","Spelers"};
    Context mCon;
    FragmentManager fm;

    public FragmentPagerAdapter(FragmentManager fm, Context c, ArrayList<Fragment> fragments) {
        super(fm);
        this.fm = fm;
        this.mCon = c;
        this.fragments = fragments;
    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }

    public View getTabView(int position){
        View v = LayoutInflater.from(mCon).inflate(R.layout.custom_tab, null);
        TextView tv = (TextView) v.findViewById(R.id.textView);
        tv.setText(tabTitles[position]);
        return v;
    }
}

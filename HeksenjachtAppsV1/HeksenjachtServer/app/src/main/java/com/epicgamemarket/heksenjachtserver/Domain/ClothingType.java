package com.epicgamemarket.heksenjachtserver.Domain;

import java.io.Serializable;

/**
 * Created by Kai on 6-10-2016.
 */

public enum ClothingType  implements Serializable {
    Hat, Shirt, Pants, Shoes
}

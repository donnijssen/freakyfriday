package com.epicgamemarket.heksenjachtserver;

import android.app.ActionBar;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.epicgamemarket.heksenjachtserver.Adapters.FragmentPagerAdapter;
import com.epicgamemarket.heksenjachtserver.Controllers.GameController;
import com.epicgamemarket.heksenjachtserver.Fragments.ControlsFragment;
import com.epicgamemarket.heksenjachtserver.Fragments.PlayersFragment;
import com.epicgamemarket.heksenjachtserver.Fragments.StatsFragment;

import java.util.ArrayList;

public class MainTabActivity extends AppCompatActivity {

    ViewPager viewPager;
    TabLayout tabLayout;
    ActionBar actionBar;
    public static GameController gameController = new GameController();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_tab);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        tabLayout = (TabLayout) findViewById(R.id.sliding_tabs);
        viewPager.setOffscreenPageLimit(5);
        actionBar = getActionBar();

        FragmentPagerAdapter fpa = new FragmentPagerAdapter(getSupportFragmentManager(),getApplicationContext(),getFragments());
        viewPager.setAdapter(fpa);
        tabLayout.setupWithViewPager(viewPager);

        // Iterate over all tabs and set the custom view
        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            TabLayout.Tab tab = tabLayout.getTabAt(i);
            tab.setCustomView(fpa.getTabView(i));
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    public GameController getGameController(){
        return gameController;
    }

    private ArrayList<Fragment> getFragments(){
        ArrayList<Fragment> fragments = new ArrayList<>();
        fragments.add(new ControlsFragment());
        fragments.add(new StatsFragment());
        fragments.add(new PlayersFragment());
        return fragments;
    }
}

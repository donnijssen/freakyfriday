package com.epicgamemarket.heksenjachtserver.Fragments;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.epicgamemarket.heksenjachtserver.Controllers.GameController;
import com.epicgamemarket.heksenjachtserver.MainTabActivity;
import com.epicgamemarket.heksenjachtserver.R;


public class StatsFragment extends Fragment {

    TextView tv_hints;
    TextView tv_locs;
    Button btn_witch;
    private GameController gameController;

    private int locsScanned = 0;

    public StatsFragment() {
        // Required empty public constructor
    }

  /*  // TODO: Rename and change types and number of parameters
    public static StatsFragment newInstance(String param1, String param2) {
        StatsFragment fragment = new StatsFragment();
        return fragment;
    }*/

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_stats, container, false);
        tv_hints = (TextView) v.findViewById(R.id.tv_hintInfo);
        tv_locs = (TextView) v.findViewById(R.id.tv_locInfo);
        btn_witch = (Button) v.findViewById(R.id.btn_witch);
        gameController = ((MainTabActivity)getActivity()).getGameController();


        btn_witch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        getContext());
                alertDialogBuilder
                        .setCancelable(false)
                        .setPositiveButton("Ok",new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.dismiss();
                            }
                        });
                if (gameController.isGameStarted()&& gameController.getPlayerController().getWitch()!=null){
                    alertDialogBuilder.setMessage("Heks: "+ gameController.getPlayerController().getWitch().getName());
                } else{
                    alertDialogBuilder.setMessage("Heks is niet bekend");
                }
                alertDialogBuilder.create().show();
            }
        });

        getContext().registerReceiver(mMessageReceiver, new IntentFilter("GETDATA"));
        return v;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getContext().unregisterReceiver(mMessageReceiver);
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {

        @Override

        public void onReceive(Context context, Intent intent) {

            boolean hintCollected = intent.getBooleanExtra("hintCollect",false);
            if (hintCollected){
                locsScanned++;
                tv_hints.setText(locsScanned +" locatie(s) gescand. Hints gekregen: "+ gameController.getLocationController().getNoOfCorrectHintsFound()+".\nResterende hints: "+ gameController.getLocationController().getNoOfHints());
            }

      /*      String locationDisabled = intent.getStringExtra("locDisabled");
            if (locationDisabled!=null && !locationDisabled.isEmpty()) {
                tv_locs.setText(GameController.getLocationInfo());
            }*/

            String locInfo = intent.getStringExtra("locInfo");
            if (locInfo != null && !locInfo.isEmpty()){
                tv_locs.setText(gameController.getLocationController().getLocationInfo());
            }

        }

    };
}

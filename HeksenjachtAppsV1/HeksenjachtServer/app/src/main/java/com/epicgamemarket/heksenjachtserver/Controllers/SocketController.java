package com.epicgamemarket.heksenjachtserver.Controllers;

import android.util.Log;

import com.epicgamemarket.heksenjachtserver.Domain.Player;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.ArrayList;



/**
 * Created by Kai on 10-10-2016.
 */

public class SocketController {



    public static void sendMsgToAllPlayers(String msg,ArrayList<Player> players) throws IOException {
        for (Player p : players) {
            OutputStream outputStream = p.getSocket().getOutputStream();
            PrintStream printStream = new PrintStream(outputStream);
            printStream.println(msg);
            //printStream.close();
            printStream.flush();
            Log.i("msg_to_all","Message to all: "+msg);
        }
    }

    public static void sendMsgToPlayer(String msg, Player p) throws IOException {
        if (p == null || p.getSocket() == null) {
            return;
        }
        OutputStream outputStream = p.getSocket().getOutputStream();
        PrintStream printStream = new PrintStream(outputStream);
        printStream.println(msg);
        printStream.flush();
        Log.i("msg_to_one","Message to: "+p.getName()+". Msg: "+msg);
        //printStream.close();
    }

}
